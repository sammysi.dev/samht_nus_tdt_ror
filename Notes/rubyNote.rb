#Ruby doesnot have "return", it returns the last statement in function
#1 - "Here Document" refers to build strings from multiple lines.
print <<EOF
   This is the first way of creating
   here document ie. multiple line string.
EOF
puts "-------------------------------------------------------------";
#2 - Declares code to be called at the beginning/end of the program.
puts "This is main Ruby Program"

END {
   puts "Terminating Ruby Program"
}
BEGIN {
   puts "Initializing Ruby Program"
}
#comment single line
#comment multiple lines
=begin
	Comment
	Multiple
	Lines
=end
puts "-------------------------------------------------------------";
#3 - VARIABLE
###LOCAL###
puts "LOCAL VARIABLE"
local_variable = 'Toi nam ngoai method.'
def variable_scope
    puts local_variable = 'Toi nam trong method.'
end
variable_scope #Toi nam trong method
puts local_variable #Toi nam ngoai method


puts "--------------------";
###GLOBAL###
puts "GLOBAL VARIABLE"
$global_variable = 'Toi chua duoc thay doi.'
def variable_scope
    puts $global_variable = 'Toi da bi thay doi.'
end
variable_scope #Toi da bi thay doi.
puts $global_variable #Toi da bi thay doi.


puts "--------------------";
###INSTANCE###
puts "INSTANCE VARIABLE"
class Nguoi
  def initialize(ten)
    @ten = ten
  end

  def show
    puts @ten
  end
end

first = Nguoi.new('Nguyen')
first.show # Nguyen

second = Nguoi.new('Anh')
second.show # Anh


puts "--------------------";
###CLASS###
puts "CLASS VARIABLE"
class Dog

  def initialize(leg)
    @@leg = leg
  end

  def show_leg
    puts @@leg
  end

end

first = Dog.new(4)
first.show_leg # 4

second = Dog.new(10)
second.show_leg # 10

first.show_leg # 10
puts "-------------------------------------------------------------";
puts "CONSTANT VARIABLE"
A_CONST = 10
A_CONST = 20
puts "-------------------------------------------------------------";
#4 - OPERATORS
#** exponent (power)
#== check boolean true,false
#<=> check equal->0, greater->1, less->-1
#.eql? 1 == 1.0 returns true ~~ 1.eql?(1.0) return false.
#1.fdiv 2 = 0.5 (fdiv on 2 Intergers -> result: Float with (.) )
#12.to_f = 12.0 (to_f => Float)
#sprintf "%.4f" % (1/3.0) = 0.3333
#to_r (rational number) : 1.5.to_r
#to_s (to String)