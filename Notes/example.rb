#combo each + next
def sum array
  sum = 0
  if array.length > 0
    array.each do |item|
      if item < 0
        next
      else
        sum += item
      end
    end
  end
  sum
end
array = [1,10,-5,-9,11]
puts sum array

#Sample
def sum array
  total = 0
  array.each do |num|
    next if num <= 0
    total += num
  end
  total
end

#-----------------------------------------------------------------------
#Combo map + sum
array = [-1,3,4,5,-3]
def sum array
  if array.length > 0
    array.map do |item|
      if item > 0
        item
      end
    end
  end
end
a = (sum array).compact
puts a.sum

#Sample
def sum array
  array.map do |num|
    num <= 0 ? 0 : num
  end.sum
end

#-----------------------------------------------------------------------
#Each || MAP
arr = ["Jane","Elsa","Bobb"]
#Each return value original array["Jane","Elsa","Bobb"]
def testEach arr
  arr.each do |item|
    puts "in function testEach: I love #{item}"
  end
end
#Each return changing value {"I love Jane","I love Elsa","I love Bobb"}
def testEachChangeValue arr
  myStatement = []
  arr.each do |item|
    myStatement << "I love #{item}"
  end
  myStatement
end
#Map return a new array[]
arr1 = [3,4,6]
def testMap arr
  arr.map do |item|
    item += 2
  end
end
puts testEach arr
puts testEachChangeValue arr
puts testMap arr1

#-----------------------------------------------------------------------
#Cau1
class Person
  attr_reader :name, :age
  attr_writer :age
  def initialize name,age
    @name = name
    @age = age
  end
end
#Cau2
class Person
  attr_reader :first_name, :last_name
  def initialize fname,lname
    @first_name = fname
    @last_name = lname
  end
  def full_name
    #first_name + " " + last_name (reason: having attr_reader) (1)
    #self.first_name + " " + self.last_name (2)
    #(1)(2) difference
    #@first_name.concat(" "+@last_name)
    "#{first_name} #{last_name}"
  end
end

#-----------------------------------------------------------------------
#1
def subtract a b
  a - b
end

#2
def subtract a,b=0
  a - b
end

#7
class People
  attr_accessor :name,:age
  def initialize name,age
    @name = name
    @age = age
  end
end

#8
class People
  attr_reader :age
  def initialize name,age
    @name = name
    @age = age
  end
end

#15-16 https://viblo.asia/p/difference-between-vs-and-equal-vs-eql-in-ruby-07LKXwoP5V4
#16
class TestE
  attr_accessor :value
  def hash()
    @value % 2
  end
  def eql?(other)
    @value.hash = other.hash
  end
end

#17
=begin
a = :test
b = nil
#and
result = a and b

  => nil
  result #=> :test

#&&
result1 = a && b

  =>nil
  result1 #=> nil


#and (result) <=> #&& (result1)
result = (a and b)

#&& (result1) <=> #and (result)
(result1 = a) && b
=end

#19
MR_COUNT = 0

module Foo
  MR_COUNT = 1
end

class Bar
  MR_COUNT = 2

  def practice
    puts "constant class Bar, MR_COUNT = #{MR_COUNT}"
    puts "constant module Foo, MR_COUNT = #{Foo::MR_COUNT}"
    puts "constant global enviroment, MR_COUNT = #{::MR_COUNT}"
  end
end

#20
def test a
  if a > 18
    true
  else
    false
  end
end

def test1 a
  unless a > 18
    true
  else
    false
  end
end

def test2 grade
  case grade
  when 0...3.5
    "HsKem"
  when 3.5...5
    "HsYeu"
  when 5...6.5
    "HsTrungBinh"
  when 6.5...7
    "HsTrungBinhKha"
  when 7...8
    "HsKha"
  when 8..10
    "HsGioi"
  else
    "No school"
  end
end

#21
i = 0
while i < 10
  puts i
  i = i+1
end

i = 0
until i == 10
  puts i
  i = i + 1
end

array = [1,2,3]
for i in 0 ... array.size
  puts array[i]
end

array.each {|x| puts x}

def test
  loop do
    puts "Input an odd number: "
    input = gets.chomp.to_i
    if input%2 == 0
      break;
    end
  end
end

#22
for i in 0..10
  #if odd number -> next
  if i % 2 != 0
    next
  else
    puts i
  end
end

for i in 0..10
  if i > 8
    break
  else
    puts i
  end
end

###28
names = "nga trung hoa hiền nga trung hoa hiền kiên hà hải"
#1
names.size
names.length
#2
names[0,3]

names[-3,3]
names.chomp(names[-3,3])
#3

#4
names.rjust(53,"vinh")
names.prepend("vinh")
#5
names.ljust(53,"vinh")
names.insert(-1,"vinh")
#7
names.gsub("nga","nhân")
names.gsub!("nga","nhân")
#8
names.sub(names[3,7],"nhân")
names.sub!(names[3,7],"nhân")
#9
names.delete "nga"
names.delete! "nga"
#10
names.capitalize
names.capitalize!
#11
names.upcase
names.upcase!
#12
names.downcase
names.downcase!
#13
names.include?"dương"
#14
names.empty?
#15
names.tr(" ","")
names.tr!(" ","")

###30
nums = [7, 9, 22, 4, 5, 7, 8, nil, 100, -7, 9, 0, nil, nil]
#1
nums.size
#2
nums.count {|x| x != nil && x % 2 == 0}
#3
nums.first
nums.last
#4
nums.first(5)
nums.last(5)
#5
nums[2,4]
#6
nums.unshift 14,11,97,9
nums.push 3,6,97,9
#7
nums.insert 2,21,5,18
#8
nums.shift
#9
nums.pop
#10
nums.delete 9
#11
nums.delete_at(5)
#12
nums.each do |item|
  next if item == nil
  puts item *= 3
end
#13
nums.each_with_index do |item,i|
  next if item == nil
  puts "#{i} => #{item *= 3}"
end

###31
nums = [7, 9, 22, 4, 5, 7, 8, nil, 100, -7, 9, 0, nil, nil]
#2
nums.empty?
#3
nums.include?(23)
#4
nums.compact
nums.reject(&:nil?)
nums.reject! { |item| item.nil? }
#5
nums.uniq
nums.uniq!
#7
nums.map { |item| item *= 5 if item != nil}
#8
nums.map! { |item| item += 2 if item != nil}
#9
#nil remains
nums.compact.map { |item| item if item > 18}
#new value - not nil
#???
#10
#nil remains
nums.reject! { |item| item if item != nil && item <= 18 }
#new value - not nil
#???
#11
nums.compact.join(",")
##SORTING
nums.reject! { |item| item.nil? }
#12
nums.sort! #change original array
#13
nums.sort_by! { |x| -x}
#14
nums.sort! { |x| x%3}

###32
mang1 = [1, 2, 5, 7]
mang2 = [5, 7, 10, 12]
#1
#mang1.select{ |m1| mang2.none?(m1)}.map{ |m1| m1}
mang1 - mang2
#2
#mang1.select{ |m1| mang2.include?(m1)}.map{ |m1| m1}
mang1 & mang2
#3
mang1 | mang2
##Compare
m1 = [1,2,3]
m2 = [1,2,3]
m3 = [2,1,3,3]
m4 = [2,1,3]
#4
def check a,b
  a.sort == b.uniq.sort
end
check m1,m3
check m1,m4
#5
def compare a,b
  a.eql? b
end
compare m1,m2
compare m1,m3
compare m1,m4

###34
t = Time.now
#1
t.wday
t.day
t.mon
t.year
t.hour
t.min
t.sec
#2
t.monday?
#3
t + (60*2+30)
#4
t - (60*60*24*3 + 2)
#5
t.strftime("%Y/%m/%d %H:%M:%S %A")

###40
status_mapping = {pending: 0, approved: 1, declined: 2}
#1
status_mapping.each { |x| x}
#2
status_mapping.map { |key,val| key}
#3
status_mapping.map { |key,val| val}
#4
status_mapping.each do |key,val|
  puts key if val == 1
end
#5
status_mapping.each do |key,val|
  puts val if key == :declined
end
#6
status_mapping.delete(:pending)
#7
status_mapping.reject! { |key,val| val % 2 + 3 / 700 > 7}
#8
status_mapping.key?(:not_response)(
#9
status_mapping.value?(3)
#10
status_mapping[:not_response] = 3

###63
class TestInput
  def initialize *args
    if args.size < 2 || args.size > 5
      puts "Please input 2 to 5 parameters only"
    else
      puts "You input #{args.size} parameters"
    end
  end
end

###64
class Animal
  attr_reader :head, :tail, :ears
  def initialize
    @head = 1
    @tail = 1
    @ears = 2
  end
end

class Snake < Animal
  def initialize
    super
    @ears = 0
  end
end

snake = Snake.new
puts snake.ears
animal = Animal.new
puts animal.ears

#69
class Person
  def initialize name
    @name = name
  end
  def print
    @name
  end
end
sinhvien = Person.new "Ha Thuy Sam"

#76
class Driver
  public
    def public_method
      puts "public_method called"
      private_method
      protected_method

      private_method
      self.protected_method
    end
  private
    def private_method
      puts "private_method called"
    end
  protected
    def protected_method
      puts  "protected_method called"
    end
end
d = Driver.new
d.public_method()

###79
module P1
  def testing1
    "parent testing"
  end
end
class Parent1
  include P1
end

module P2
  def testing2
    "parent testing"
  end
end
class Parent2
  include P2
end
class Child
  include P1
  include P2
end

###80
module Mixin
  def printGrade
    10.0
  end
end
class Student
  include Mixin
end

###83
begin
  1/0
rescue => e
  puts "#{e.class.name}"
  puts "#{e.message}"
ensure
  puts "Run when?"
end