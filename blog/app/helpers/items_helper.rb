module ItemsHelper
  def check_radio_button (showing)

    if params[:showing].blank?
      radio_button_tag(:showing, showing)
    elsif showing == params[:showing]
      radio_button_tag(:showing, showing, :checked => true)
    else
      radio_button_tag(:showing, showing)
    end

  end
end
