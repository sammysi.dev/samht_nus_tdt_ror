class Item < ApplicationRecord
  belongs_to :list
  scope :by_lists, -> list { where(:list => list) }

  validates :title, :description, :due_date, presence: true
  validates :title, uniqueness: { scope: :list_id,
    message: "duplicate in one list" }

  after_create do
    self.update_columns finish: "0"
  end

end