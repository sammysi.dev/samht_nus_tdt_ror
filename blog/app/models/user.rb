class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  has_many :lists
  #has_and_belongs_to_many :lists (if use original table name will confuse rails which "lists")
  has_and_belongs_to_many :shared_lists, class_name: "List", join_table: "lists_users"
  #If use a custom table name instead of original table name, rails will understand that join_table is users_shared_lists)

  validates :full_name, :dob, presence: true
  validates_confirmation_of :password

  after_save do
    self.update_columns age: Time.now.year - dob.year
  end

  def self.search(search)
    if search
      where('lists.list_name LIKE ?', "%#{search}%")
    else
      all
    end
  end

end
