class List < ApplicationRecord
  belongs_to :user
  has_many :items, dependent: :destroy
  has_and_belongs_to_many :participants, class_name: "User", join_table: "lists_users"

  validates :list_name, presence: true, uniqueness: { scope: :user_id,
    message: "duplicate !!" }, on: :create

  def self.search(search)
    if search
      where('lists.list_name LIKE ?', "%#{search}%")
    else
      all
    end
  end
end