class UsersController < ApplicationController

  def index
    #Show all shared_lists
    @lists = List.joins("inner join lists_users on lists.id = lists_users.list_id").where("lists_users.user_id = ?",current_user.id).order(created_at: :desc).paginate(:page => params[:page], :per_page => 3).search(params[:search])
  end

end
