class WelcomeController < ApplicationController

  def index
    #Show count of items and lists
    @count_item = Item.joins(list: :user).where("user_id = ?",current_user.id).size
    @count_list = List.all.size
  end

end
