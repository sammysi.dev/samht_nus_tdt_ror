class ListsController < ApplicationController
  before_action :load_list_update, only: [:edit, :update, :destroy]
  before_action :load_list_create, only: [:new, :create]

  def index
    @lists = List.where("lists.user_id = ?",current_user.id).order(created_at: :desc).paginate(:page => params[:page], :per_page => 3).search(params[:search])
  end


  def create
    @list = List.new(list_params)
    @list.user_id = current_user.id

    if @list.save
      redirect_to lists_path, notice: "List was successfully created."
    else
      render 'new', alert: "ERROR!"
    end
  end

  def update
    if @list.update list_params
      redirect_to lists_path, notice: "List was successfully updated."
    else
      render 'edit', alert: "ERROR!"
    end
  end

  def destroy
    @list.destroy
    redirect_to lists_path, notice: "List was destroyed."
  end

private

  def load_list_update
    @list = List.find(params[:id])
  end

  def load_list_create
    @list = List.where(:user_id => current_user.id).new
  end

  def list_params
    params.require(:list).permit(:list_name, :user_id, { participant_ids:[] })
  end

end
