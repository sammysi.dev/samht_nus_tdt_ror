class ItemsController < ApplicationController
  before_action :load_item_update, only: [:edit, :update, :destroy]
  before_action :load_item_create, only: [:new, :create]

  has_scope :by_lists, type: :array

  def index
    today = Time.now

    if params[:user_id]
      #Show 3 options in select box - Items of Shared Lists
      show_shared_lists_items

    elsif params[:list_id]
      #Show 3 options in select box - Items of Lists
      show_lists_items

    else
      #Show Items in dashboard with filter List
      @items = apply_scopes(Item).joins(list: :user).where("users.id = ? and finish = ? and due_date <= ?",current_user.id,0,today.end_of_day).order(due_date: :desc).paginate(:page => params[:page], :per_page => 6)
    end
  end

  def finish
    #Update unfinished item -> finished
    @item = Item.find(params[:id])
    @item.update_attribute(:finish, 1)

    #get user_id to redirect
    @user = List.find(@item.list_id).user_id

    unless @user == current_user.id
      redirect_to user_items_path(@item.list_id), notice: "Item was finished!"
    else
      redirect_to list_items_path(@item.list_id), notice: "Item was finished!"
    end
  end

  def create
    @item = Item.new(item_params)
    #get user_id to redirect
    @user = List.find(@item.list_id).user_id

    if @item.save
      if @user == current_user.id
        #List_items
        redirect_to list_items_path(@item.list_id), notice: "Item was successfully created."
      else
        #Shared_List_items
        redirect_to user_items_path(@item.list_id), notice: "Item was successfully created."
      end
    else
      render 'new', alert: "ERROR!"
    end
  end

  def update
    @user = List.find(@item.list_id).user_id

    if @item.update(item_params)
      if @user == current_user.id
        #List_items
        redirect_to list_items_path(@item.list_id), notice: "Item was successfully updated."
      else
        #Shared_List_items
        redirect_to user_items_path(@item.list_id), notice: "Item was successfully updated."
      end
    else
      render 'edit', alert: "ERROR!"
    end
  end

  def destroy
    @item.destroy
    redirect_to list_items_path(@item.list_id), notice: "Item was destroyed."
  end

private

  def load_item_create

    if params[:list_id]
      @item = Item.where(:list_id => params[:list_id]).new
    else
      @item = Item.where(:list_id => params[:user_id]).new
    end

  end

  def load_item_update

    @item = Item.find(params[:id])
    @user = List.find(@item.list_id).user_id

  end

  def show_shared_lists_items
    if params[:showing] == "All"
      @items = Item.joins(list: :user).where("list_id = ? and users.id != ?",params[:user_id],current_user.id).order(due_date: :desc).paginate(:page => params[:page], :per_page => 6)
    elsif params[:showing] == "Finished"
      @items = Item.joins(list: :user).where("users.id != ? and finish = ? and list_id = ?",current_user.id,1,params[:user_id]).order(due_date: :desc).paginate(:page => params[:page], :per_page => 6)
    elsif params[:showing] == "Unfinished"
      @items = Item.joins(list: :user).where("users.id != ? and finish = ? and list_id = ?",current_user.id,0,params[:user_id]).order(due_date: :desc).paginate(:page => params[:page], :per_page => 6)
    else
      @items = Item.joins(list: :user).where("list_id = ? and users.id != ?",params[:user_id],current_user.id).order(due_date: :desc).paginate(:page => params[:page], :per_page => 6)
    end
  end

  def show_lists_items
    if params[:showing] == "All"
      @items = Item.joins(list: :user).where("list_id = ? and users.id = ?",params[:list_id],current_user.id).order(due_date: :desc).paginate(:page => params[:page], :per_page => 6)
    elsif params[:showing] == "Finished"
      @items = Item.joins(list: :user).where("users.id = ? and finish = ? and list_id = ?",current_user.id,1,params[:list_id]).order(due_date: :desc).paginate(:page => params[:page], :per_page => 6)
    elsif params[:showing] == "Unfinished"
      @items = Item.joins(list: :user).where("users.id = ? and finish = ? and list_id = ?",current_user.id,0,params[:list_id]).order(due_date: :desc).paginate(:page => params[:page], :per_page => 6)
    else
      @items = Item.joins(list: :user).where("list_id = ? and users.id = ?",params[:list_id],current_user.id).order(due_date: :desc).paginate(:page => params[:page], :per_page => 6)
    end
  end

  def item_params
    params.require(:item).permit(:list_id, :title, :description, :due_date)
  end

end