$(document).on('turbolinks:load', function() {
  //Datepicker
  $('.datepicker').datepicker({
      format: "dd-mm-yyyy",
      orientation: "bottom left",
      autoclose: true,
      todayHighlight: true,
      toggleActive: true,
  });

  //Show more - less
  var title = 20;
  var description = 164;
  var ellipsestext = "...<br/>";
  var moretext = "show more";
  var lesstext = "show less";

  $('.titleControl').each(function() {
      var content = $(this).html();

      if(content.length > title) {

          var c = content.substr(0, title);
          var h = content.substr(title, content.length - title);

          var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

          $(this).html(html);
      }
  });

  $('.descriptionControl').each(function() {
      var content = $(this).html();

      if(content.length > description) {

          var c = content.substr(0, description);
          var h = content.substr(description, content.length - description);

          var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink ml">' + moretext + '</a></span>';

          $(this).html(html);
      }
  });

  $(".morelink").click(function(){
      if($(this).hasClass("less")) {
          $(this).removeClass("less");
          $(this).html(moretext);
      } else {
          $(this).addClass("less");
          $(this).html(lesstext);
      }
      $(this).parent().prev().toggle();
      $(this).prev().toggle();
      return false;
  });

  //Filter by list

  /*$('input[type=checkbox').change(function() {
    $.get($('#lists_search').attr('action'),
    $('#lists_search').serialize(), null, 'script');

    return false;
  });*/
  $('#lists_search').submit(function () {
      $.get(this.action, $(this).serialize(), null, 'script');
      return fasle;
  });

});
