Rails.application.routes.draw do
  get 'welcome/index'
  root 'welcome#index'

  devise_for :users, :controllers => { registrations: 'registrations' }
  resources :devise

  resources :users, except: [:show, :destroy] do
    resources :items
  end

  resources :lists do
    resources :items
  end

  resources :items do
    member do
      patch :finish
      put :finish
    end
  end

end
