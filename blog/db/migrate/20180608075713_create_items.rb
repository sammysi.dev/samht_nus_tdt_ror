class CreateItems < ActiveRecord::Migration[5.2]
  def change
    create_table :items do |t|
      t.belongs_to :list, index: true
      t.string :title
      t.text :description
      t.binary :finish
      t.date :due_date
      t.timestamps
    end
  end
end
