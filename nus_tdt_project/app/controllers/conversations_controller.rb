class ConversationsController < ApplicationController
  before_action :set_conversation, only: [:close]

  def create
    @conversation = Conversation.get(current_user.id, params[:user_id])

    add_to_conversations unless conversated?

    respond_to do |format|
      format.js
    end
  end

  def close
    session[:conversations].delete(@conversation.id)

    respond_to do |format|
      format.js
    end
  end

  private

    def add_to_conversations
      session[:conversations] ||= []
      session[:conversations] << @conversation.id
    end

    def conversated?
      session[:conversations].include?(@conversation.id)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_conversation
      @conversation = Conversation.find(params[:id])
    end
end
