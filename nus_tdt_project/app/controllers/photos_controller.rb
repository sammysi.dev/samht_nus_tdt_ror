class PhotosController < ApplicationController
  include AuthorizeAdmin

  skip_before_action :authenticate_user!, only: [:show]
  before_action :authenticate_admin, only: [:index]
  before_action :load_photo, only: [:edit, :update, :destroy]

  def index
    @photos = Photo.no_album.newest.page(params[:page]).per(40)
  end

  def show
    @photo = Photo.find(params[:id])
    respond_to do |format|
      format.js { render 'modal' }
    end
  end

  def new
    @photo = Photo.new
  end

  def create
    @photo = current_user.photos.new(photo_params)
    if @photo.save
      redirect_to user_path(current_user), notice: t('.create_photo')
    else
      flash.now[:error] = @photo.errors.full_messages.join("<br />").html_safe
      render :new
    end
  end

  def update
    if @photo.update(photo_params)
      redirect_to after_update_destroy_path, notice: t('.update_photo')
    else
      flash.now[:error] = @photo.errors.full_messages.join("<br />").html_safe
      render :edit
    end
  end

  def destroy
    @photo.destroy
    redirect_to after_update_destroy_path, notice: t('.destroy_photo')
  end

  def like
    @photo = Photo.find(params[:id])
    current_user.like(@photo)
    respond_to do |format|
      format.json { render json: { count: @photo.likes.count, id: params[:id] } }
    end

    # broadcasting likes using pusher
    Pusher.trigger('comment-channel','like-count', {
      id: @photo.id,
      like: @photo.likes.size
    })
  end

  private

    def after_update_destroy_path
      current_user.admin? ? photos_path : user_path(current_user)
    end

    def load_photo
      if current_user.admin?
        @photo = Photo.find(params[:id])
      else
        @photo = current_user.photos.find(params[:id])
      end
    end

    def photo_params
      params.fetch(:photo, {}).permit(:user_id, :title, :description, :mode, :image, :album_id)
    end
end
