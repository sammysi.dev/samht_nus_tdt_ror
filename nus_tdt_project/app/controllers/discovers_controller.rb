class DiscoversController < ApplicationController
  before_action :load_user, except: [:index]

  def index
    @photos = Photo.newest_public_photo_without(current_user).page(params[:page])
    @albums = Album.newest_public_album_without(current_user).page(params[:page])
  end

  def follow_user
    if current_user.follow @user.id
      respond_to do |format|
        format.js
      end
    end
  end

  def unfollow_user
    if current_user.unfollow @user.id
      respond_to do |format|
        format.js
      end
    end
  end

  private

    def load_user
      @user = User.find(params[:id])
    end
end
