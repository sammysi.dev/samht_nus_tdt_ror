class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception
  before_action :authenticate_user!
  before_action :set_user_list

  def after_inactive_sign_up_path_for(resource)
    new_user_session_path
  end

  def after_sign_in_path_for(resource)
    root_path
  end

  def set_user_list
    if user_signed_in?
      @followers = current_user.followers
      @following = current_user.following
      session[:conversations] ||= []
      @conversations = Conversation.includes(:recipient, :messages).find(session[:conversations])
    end
  end

end
