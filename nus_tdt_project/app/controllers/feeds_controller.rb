class FeedsController < ApplicationController
  skip_before_action :authenticate_user!

  def index
    if user_signed_in?
      if current_user.following.exists?
        @photos = Photo.no_album.following_users_of(current_user).search(params[:search]).page(params[:page])
        @albums = Album.include_photo.following_users_of(current_user).search(params[:search]).page(params[:page])
      else
        @photos = Photo.newest_public_photo_without(current_user).search(params[:search]).page(params[:page])
        @albums = Album.newest_public_album_without(current_user).search(params[:search]).page(params[:page])
      end
    else
      @photos = Photo.no_album.newest_public.search(params[:search]).page(params[:page])
      @albums = Album.include_photo.newest_public.search(params[:search]).page(params[:page])
    end
  end

end
