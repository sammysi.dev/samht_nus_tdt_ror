class MessagesController < ApplicationController

  def create
    @conversation = Conversation.includes(:recipient).find(params[:conversation_id])
    @message = @conversation.messages.create(message_params)

    respond_to do |format|
      format.js
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def message_params
      params.fetch(:message, {}).permit(:user_id, :body)
    end
end
