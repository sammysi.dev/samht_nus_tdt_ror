class UsersController < ApplicationController
  include AuthorizeAdmin

  before_action :authenticate_admin, except: [:show]
  before_action :load_user, only: [:show, :edit, :update, :destroy]

  def index
    @users = User.non_admin.newest
  end

  def show
    @photos = @user.photos.no_album.newest.page(params[:page])
    @albums = @user.albums.include_photo.newest.page(params[:page])
    @notifications = Notification.all.reverse
  end

  def update
    send_deactivated_notification(@user.id)
    if update_user
      redirect_to users_path, notice: "User was successfully updated."
    else
      flash.now[:error] = @user.errors.full_messages.join("<br />").html_safe
      render :edit
    end
  end

  def destroy
    send_deleted_notification(@user.id)
    @user.destroy
    redirect_to users_path, notice: "User was successfully destroyed."
  end

  private

    def update_user
      if user_params[:password].present?
        @user.update(user_params)
      else
        user_params_without_password = user_params.except(:password)
        @user.update(user_params_without_password)
      end
    end

    def send_deactivated_notification(id)
      if user_params[:deactive] == "1"
        UserMailer.deactive_user_notification(id).deliver_later unless @user.deactive?
      end
    end

    def send_deleted_notification(id)
      UserMailer.delete_user_notification(id).deliver_later
    end

    def load_user
      @user = User.find(params[:id])
    end

    def user_params
      params.fetch(:user, {}).permit(:avatar, :first_name, :last_name, :email, :password, :deactive, :active)
    end
end
