class AlbumsController < ApplicationController
  include AuthorizeAdmin

  skip_before_action :authenticate_user!, only: [:show]
  before_action :authenticate_admin, only: [:index]
  before_action :load_album, only: [:edit, :update, :destroy]

  MAXIMUM_PHOTOS = 25
  MINIMUM_PHOTOS = 1

  def index
    @albums = Album.include_photo.newest.page(params[:page]).per(40)
  end

  def show
    @album = Album.find(params[:id])
    respond_to do |format|
      format.js { render 'modal' }
    end
  end

  def new
    @album = Album.new
  end

  def create
    @album = current_user.albums.new(album_params)

    return one_photo_error unless params[:images]

    if @album.save
      create_photos
      redirect_to edit_user_album_path(current_user,@album), notice: t('.create_album')
    else
      flash.now[:error] = @album.errors.full_messages.join("<br />").html_safe
      render :new
    end
  end

  def update
    if @album.update(album_params)

      if params[:images]
        # Album has 25 photos only
        if (@album.photos.size + params[:images].length) > MAXIMUM_PHOTOS
          return maximum_photos_error
        else
          create_photos
        end

      elsif params[:delete_ids].present?
          # Albums has at least 1 photo
          ids = params[:delete_ids][0].split(",")
          if (@album.photos.size - ids.length) < MINIMUM_PHOTOS
            return one_photo_error
          else
            @album.photos.where(id: ids).destroy_all
          end
      end

      redirect_to after_update_destroy_path, notice: t('.update_album')
    else
      flash.now[:error] = @album.errors.full_messages.join("<br />").html_safe
      render :edit
    end
  end

  def destroy
    @album.destroy
    redirect_to after_update_destroy_path, notice: t('.destroy_album')
  end

  def like
    @album = Album.find(params[:id])
    current_user.like(@album)
    respond_to do |format|
      format.json { render json: { count: @album.likes.count, id: params[:id] } }
    end

    # broadcasting likes using pusher
    Pusher.trigger('comment-channel','like-count', {
      id: @album.id,
      like: @album.likes.size
    })
  end

  private

    def after_update_destroy_path
      current_user.admin? ? albums_path : user_path(current_user, tab: "album")
    end

    def create_photos
      params[:images].each { |photo|
        @album.photos.create(
          image: photo,
          user: current_user,
          title: @album.title,
          description: @album.description )}
    end

    def maximum_photos_error
      flash.now[:alert] = t('.maximum_photos')
      render :edit
    end

    def one_photo_error
      flash.now[:alert] = t('.minimum_photos')
      render :new
    end

    def load_album
      if current_user.admin?
        @album = Album.find(params[:id])
      else
        @album = current_user.albums.find(params[:id])
      end
    end

    def album_params
      params.fetch(:album, {}).permit(:user_id, :title, :description, :mode, :photos, :delete_ids)
    end
end
