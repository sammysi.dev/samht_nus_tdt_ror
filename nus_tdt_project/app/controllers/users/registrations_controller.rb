class Users::RegistrationsController < Devise::RegistrationsController
  before_action :configure_sign_up_params, only: [:create]
  before_action :configure_account_update_params, only: [:update]

  def update
    respond_to do |format|
      # Omniauth accounts without current_password - Normal accounts with current_password
      if @user.provider? ? @user.update(account_update_params) : @user.update_with_password(account_update_params)
        bypass_sign_in(@user) # Sign in with new password
        if @user.admin?
          format.html { redirect_to root_path, notice: 'User was successfully updated.' }
        else
          format.html { redirect_to @user, notice: 'User was successfully updated.' }
        end
      else
        format.html { render :edit }
      end
    end
  end

  protected

    def configure_sign_up_params
      devise_parameter_sanitizer.permit(:sign_up, keys: [:email, :password, :password_confirmation, :first_name, :last_name])
    end

    def configure_account_update_params
      devise_parameter_sanitizer.permit(:account_update, keys: [:email, :current_password, :password, :password_confirmation, :first_name, :last_name, :avatar])
    end
end
