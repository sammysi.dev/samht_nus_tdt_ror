module AuthorizeAdmin
  extend ActiveSupport::Concern

  def authenticate_admin
    return if current_user.admin?
    redirect_to root_path
  end
end
