class Photo < ApplicationRecord
  include AssociationValidationSearch, Scope

  belongs_to :album, optional: true

  scope :no_album, -> { where(album: nil) }

  scope :newest_public_photo_without, lambda { |user|
    where(album: nil, mode: "public").where.not(user: user).order(created_at: :desc)
  }

  mount_uploader :image, MyUploader
end
