class User < ApplicationRecord
  include Scope

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable, :omniauthable

  mount_uploader :avatar, MyUploader

  has_many :photos, :dependent => :destroy
  has_many :albums, :dependent => :destroy

  has_many :comments, :dependent => :destroy
  has_many :likes, :dependent => :destroy
  has_many :liked_photos, :through => :likes, :source => :likeable, :source_type => 'Photo'
  has_many :liked_albums, :through => :likes, :source => :likeable, :source_type => 'Album'

  has_many :messages
  has_many :conversations, foreign_key: :sender_id, :dependent => :destroy

  has_many :follower_relationships, foreign_key: :following_id, class_name: 'Relationship'
  has_many :followers, through: :follower_relationships, source: :follower, :dependent => :destroy

  has_many :following_relationships, foreign_key: :follower_id, class_name: 'Relationship'
  has_many :following, through: :following_relationships, source: :following, :dependent => :destroy

  validates :email, uniqueness: true
  validates_format_of :email,
    :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create

  validates :first_name,  presence: true
  validates :last_name,   presence: true, unless: :provider # Social network accounts
  validates :first_name, :last_name,
    length: {
      maximum: 25,
      too_long: "contains maximum %{count} characters"
    }
  validates :email,
    length: {
      maximum: 255,
      too_long: "contains maximum %{count} characters"
    }
  validates :password,
    length: {
      maximum: 64,
      too_long: "contains maximum %{count} characters"
    }

  scope :non_admin, -> { where(admin: false) }

  def nick_name
    "#{first_name} #{last_name.split(' ').first}"
  end

  def avatar_name
    "#{first_name.first}#{last_name.first}"
  end

  def follow(user_id)
    following_relationships.create(following_id: user_id)
  end

  def unfollow(user_id)
    if following_relationships.find_by(following_id: user_id).present?
      following_relationships.find_by(following_id: user_id).destroy
    end
  end

  def like(item)
    if likes.where(likeable: item).exists?
      likes.where(likeable: item).destroy_all
    else
      likes.create(likeable: item)
    end
  end

  # Deactive users can not sign in website
  def active_for_authentication?
    super && !deactive?
  end

  # Multiple providers - Social login
  def self.from_omniauth(auth, signed_in_resource = nil)
    user = User.where(provider: auth.provider, uid: auth.uid).first
    if user.present?
      user
    else
      # Check wether theres is already a user with the same
      # email address
      user_with_email = User.find_by_email(auth.info.email)
      if user_with_email.present?
        user = user_with_email
      else
        user = User.new
        if auth.provider == "facebook"

          user.provider = auth.provider
          user.uid = auth.uid
          user.oauth_token = auth.credentials.token

          user.first_name = auth.info.name
          user.email = auth.info.email
          # Facebook's token doesn't last forever
          user.oauth_expires_at = Time.at(auth.credentials.expires_at)
          user.skip_confirmation!
          user.save

        elsif auth.provider == "google_oauth2"

          user.provider = auth.provider
          user.uid = auth.uid
          user.oauth_token = auth.credentials.token

          user.first_name = auth.info.first_name
          user.last_name = auth.info.last_name
          user.email = auth.info.email
          # Google's token doesn't last forever
          user.oauth_expires_at = Time.at(auth.credentials.expires_at)
          user.skip_confirmation!
          user.save

        end
      end
    end
    return user
  end

  # For Twitter (save the session eventhough we redirect user to registration page first)
  def self.new_with_session(params, session)
    if session["devise.user_attributes"]
      new(session["devise.user_attributes"]) do |user|
        user.attributes = params
        user.valid?
      end
    else
      super
    end
  end

  # For Twitter (disable password validation)
  def password_required?
    super && provider.blank?
  end

end
