class Comment < ApplicationRecord
   # Associations
  belongs_to :commentable, :polymorphic => true
  belongs_to :user

  # Validations
  validates :commentable, :user, presence: true

  after_create_commit { notify }

  private
    def notify
      Notification.create(event: "<a data-remote='true' href='/users/#{commentable.user_id}/#{self.commentable_type.downcase}s/#{commentable.id}'>#{I18n.t('notifications.comments', user: self.user.nick_name, commentable: self.commentable_type)}</a>")
    end
end
