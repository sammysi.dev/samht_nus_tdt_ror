class Like < ApplicationRecord
  # Associations
  belongs_to :likeable, :polymorphic => true
  belongs_to :user

  # Validations
  validates :likeable, :user, presence: true

  after_create_commit { notify }

  private
    def notify
      Notification.create(event: I18n.t('notifications.likes', user: self.user.nick_name, likeable: self.likeable_type))
    end
end
