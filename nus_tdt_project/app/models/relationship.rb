class Relationship < ApplicationRecord
  # Assocications
  belongs_to :follower, foreign_key: 'follower_id', class_name: 'User'
  belongs_to :following, foreign_key: 'following_id', class_name: 'User'

  # Validations
  validates :follower, :following, presence: true
end
