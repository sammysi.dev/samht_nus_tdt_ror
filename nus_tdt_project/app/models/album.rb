class Album < ApplicationRecord
  include AssociationValidationSearch, Scope

  has_many :photos, :dependent => :destroy

  scope :newest_public_album_without, lambda { |user|
    includes(:photos).where.not(user: user).where(mode: "public").order(created_at: :desc)
  }
  scope :include_photo, -> { includes(:photos) }
end
