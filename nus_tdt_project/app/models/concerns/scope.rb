module Scope
  extend ActiveSupport::Concern

  included do
    scope :newest, -> { order(created_at: :desc) }

    scope :newest_public, -> { where(mode: "public").order(created_at: :desc) }

    scope :following_users_of, lambda { |user|
      joins(user: :follower_relationships)
      .where("relationships.follower_id = ? ",user.id)
      .newest_public
    }
  end

end
