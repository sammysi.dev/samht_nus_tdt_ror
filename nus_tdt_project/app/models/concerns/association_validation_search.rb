module AssociationValidationSearch
  extend ActiveSupport::Concern

  included do
    # Associations
    belongs_to :user
    has_many :likes, :as => :likeable, :dependent => :destroy
    has_many :comments, :as => :commentable, :dependent => :destroy

    # Validations
    validates :user, presence: true

    validates :title,
      length: {
        maximum: 140,
        too_long: "%{count} characters is the maximum allowed"
      },
      uniqueness: {
        allow_blank: true,
        scope: :user_id,
        case_sensitive: false
      }

    validates :description,
      length: {
        maximum: 300,
        too_long: "%{count} characters is the maximum allowed"
      }

    validates :mode, acceptance: { accept: ['public', 'private'] }

    def self.search(search)
      if search
        where('lower(title) LIKE ?', "%#{search.downcase}%")
      else
        all
      end
    end
  end

  def user_nick_name
    User.find(self.user_id).nick_name
  end
end
