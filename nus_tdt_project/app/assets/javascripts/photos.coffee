previewPhoto = ->
  $('#photo_image').on 'change', (e) ->
    readURL(this);
  $('#user_avatar').on 'change', (e) ->
    readURL(this);
  $('#images_').on 'change', (e) ->
    readURL(this);

  readURL = (input) ->
    if (input.files && input.files[0])
      reader = new FileReader()

    reader.onload = (e) ->
      $('.image-to-upload').attr('src', e.target.result).removeClass('hidden');
      $swap = $('.swap')
      if $swap
        $swap.removeClass('hidden')

    reader.readAsDataURL(input.files[0]);

$(document).on('turbolinks:load', previewPhoto)
