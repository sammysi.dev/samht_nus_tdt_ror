$(document).ready ->
  user_basic_information_rules =
    'user[email]':
      required: true
      email: true
      maxlength: 255
    'user[first_name]':
      required: true
      maxlength: 25
      minlength: 2
    'user[last_name]':
      required: true
      maxlength: 25
      minlength: 2

  user_password_rules =
    'user[password]':
      required: true
      maxlength: 64
      minlength: 6
    'user[password_confirmation]':
      required: true
      maxlength: 64
      minlength: 6
      equalTo: '#user_password'

  admin_edit_user_password_rules =
    'user[password]':
      maxlength: 64
      minlength: 6

  edit_user_password_rules =
    'user[current_password]':
      required: true
      maxlength: 64
      minlength: 6
    'user[password]':
      maxlength: 64
      minlength: 6
    'user[password_confirmation]':
      maxlength: 64
      minlength: 6
      equalTo: '#user_password'

  new_user_rules = Object.assign(user_basic_information_rules,user_password_rules)
  admin_edit_user_rules = Object.assign(user_basic_information_rules,admin_edit_user_password_rules)
  window.edit_user_rules = Object.assign(user_basic_information_rules,edit_user_password_rules)

  user_basic_information_messages =
    'user[email]':
      required: I18n.t('js.email.required')
      email: I18n.t('js.email.format')
      maxlength: I18n.t('js.maxlength',
        field: 'Email'
        count: 255)
    'user[first_name]':
      required: I18n.t('js.required', noun: 'first name')
      maxlength: I18n.t('js.maxlength',
        field: 'First name'
        count: 25)
      minlength: I18n.t('js.minlength',
        field: 'First name'
        count: 2)
    'user[last_name]':
      required: I18n.t('js.required', noun: 'last name')
      maxlength: I18n.t('js.maxlength',
        field: 'Last name'
        count: 25)
      minlength: I18n.t('js.minlength',
        field: 'Last name'
        count: 2)

  user_password_messages =
    'user[password]':
      required: I18n.t('js.password.required')
      maxlength: I18n.t('js.maxlength',
        field: 'Password'
        count: 64)
      minlength: I18n.t('js.minlength',
        field: 'Password'
        count: 6)
    'user[password_confirmation]':
      required: I18n.t('js.confirmation_password.required')
      maxlength: I18n.t('js.maxlength',
        field: 'Password confirmation'
        count: 64)
      minlength: I18n.t('js.minlength',
        field: 'Password confirmation'
        count: 6)
      equalTo: I18n.t('js.confirmation_password.equal')

  admin_edit_user_password_messages =
    'user[password]':
      maxlength: I18n.t('js.maxlength',
        field: 'Password'
        count: 64)
      minlength: I18n.t('js.minlength',
        field: 'Password'
        count: 6)

  edit_user_password_messages =
    'user[current_password]':
      required: I18n.t('js.password.required')
      maxlength: I18n.t('js.maxlength',
        field: 'Password'
        count: 64)
      minlength: I18n.t('js.minlength',
        field: 'Password'
        count: 6)
    'user[password]':
      maxlength: I18n.t('js.maxlength',
        field: 'Password'
        count: 64)
      minlength: I18n.t('js.minlength',
        field: 'Password'
        count: 6)
    'user[password_confirmation]':
      maxlength: I18n.t('js.maxlength',
        field: 'Password confirmation'
        count: 64)
      minlength: I18n.t('js.minlength',
        field: 'Password confirmation'
        count: 6)
      equalTo: I18n.t('js.confirmation_password.equal')

  new_user_messages = Object.assign(user_basic_information_messages,user_password_messages)
  admin_edit_user_messages = Object.assign(user_basic_information_messages,admin_edit_user_password_messages)
  edit_user_messages = Object.assign(user_basic_information_messages,edit_user_password_messages)

  $('#new_user').validate
    errorElement: 'div'
    errorPlacement: (error, element) ->
      error.insertBefore element
      return
    rules:
      new_user_rules
    messages:
      new_user_messages

  $('#admin_edit_user').validate
    errorElement: 'div'
    errorPlacement: (error, element) ->
      error.insertBefore element
      return
    rules:
      admin_edit_user_rules
    messages:
      admin_edit_user_messages

  $('#edit_user').validate
    errorElement: 'div'
    errorPlacement: (error, element) ->
      error.insertBefore element
      return
    rules:
      edit_user_rules
    messages:
      edit_user_messages

  new_album_rules =
    'album[title]': maxlength: 140
    'album[description]': maxlength: 300
    'images[]': required: true

  edit_album_rules =
    'album[title]': maxlength: 140
    'album[description]': maxlength: 300

  new_album_messages =
    'album[title]': maxlength: I18n.t('js.maxlength',
      field: 'Album title'
      count: 140)
    'album[description]': maxlength: I18n.t('js.maxlength',
      field: 'Album description'
      count: 300)
    'images[]': required: I18n.t('js.photo_album.required')

  edit_album_messages =
    'album[title]': maxlength: I18n.t('js.maxlength',
      field: 'Album title'
      count: 140)
    'album[description]': maxlength: I18n.t('js.maxlength',
      field: 'Album description'
      count: 300)

  $('#new_album').validate
    rules:
      new_album_rules
    messages:
      new_album_messages

  $('.edit_album').validate
    rules:
      edit_album_rules
    messages:
      edit_album_messages

  new_photo_rules =
    'photo[title]': maxlength: 140
    'photo[description]': maxlength: 300
    'photo[image]': required: true

  edit_photo_rules =
    'photo[title]': maxlength: 140
    'photo[description]': maxlength: 300

  new_photo_messages =
    'photo[title]': maxlength: I18n.t('js.maxlength',
      field: 'Photo title'
      count: 140)
    'photo[description]': maxlength: I18n.t('js.maxlength',
      field: 'Photo description'
      count: 300)
    'photo[image]': required: I18n.t('js.photo_album.required')

  edit_photo_messages =
    'photo[title]': maxlength: I18n.t('js.maxlength',
      field: 'Photo title'
      count: 140)
    'photo[description]': maxlength: I18n.t('js.maxlength',
      field: 'Photo description'
      count: 300)

  $('#new_photo').validate
    rules:
      new_photo_rules
    messages:
      new_photo_messages

  $('.edit_photo').validate
    rules:
      edit_photo_rules
    messages:
      edit_photo_messages
# ---
# generated by js2coffee 2.2.0
