# Clear URL - query string
$(document).on "turbolinks:load", ->
  url = window.location.toString()
  if url.indexOf('?') > 0
    cleanUrl = url.substring(0, url.indexOf('?'))
    window.history.replaceState {}, document.title, cleanUrl

  # Resize comment text-area
  $("#myModal").on 'shown.bs.modal', ->
    autosize($('.comment-textarea'))
