# Delete preview image
$(document).on "click", ".remove_img_preview", ->
  $(this).parent('.preview').remove()
  $(this).parent('.preview-upload-height').remove()
  addPhotoIdForDeleting($(this).data("id"))

$(document).on "turbolinks:load", ->
  # Preview images and delete all preview images
  $('#album_photos').on 'change', ->
    i = 0
    html = ""
    while i < $('#album_photos')[0].files.length
      html += "<div class='col-3 preview-upload-height p-0'><img class='image-to-upload' src='" + URL.createObjectURL(event.target.files[i]) + "'><span class='remove_img_preview'></span></div>"
      i++
    $(html).insertBefore("#image-preview")

  # Add Photo ids for deleting
  photoIds = []
  addPhotoIdForDeleting = (photoId) ->
    photoIds.push photoId
    photoIds.join(',')
    $('input#delete_ids_').val photoIds

