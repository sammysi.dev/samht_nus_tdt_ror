$(document).on "turbolinks:load", ->
  $(window).on 'keypress', (event) ->
    keycode = if event.keyCode then event.keyCode else event.which
    isShown = $('#logout-modal').hasClass('show')
    if (keycode == 13) && isShown
      $('#logout').click()

  # Close conversations
  $(document).on 'click', '.toggle-window', (e) ->
    e.preventDefault()
    card = $(this).parent().parent()
    messagesList = card.find('.messages-list')
    card.find('.card-body').toggle()
    card.attr 'class', 'card bg-dark'
    if card.find('.card-body').is(':visible')
      height = messagesList.scrollHeight
      messagesList.scrollTop height
