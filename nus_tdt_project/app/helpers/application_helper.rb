module ApplicationHelper

  def save_btn
    submit_tag "Save", class: "btn btn-outline-success btn-block"
  end

  def back_btn(user,class_name)
    if user.admin?
      link_to "Back", class_name == "Photo"? photos_path : albums_path, class: "btn btn-outline-primary btn-block"
    else
      link_to "Back", class_name == "Photo"? user_path(user) : user_path(user, tab: "album"), class: "btn btn-outline-primary btn-block"
    end
  end

  def redirect_tab(tab)
    if tab
      "active"
    else
      ""
    end
  end

  # For Update Photo/Album
  def delete_btn(object, class_name = "Photo")
    unless object.id.nil?
      if class_name == "Album"
        link_to "Delete", user_album_path(current_user,object), class: "btn btn-outline-danger btn-block",data: { confirm: "Delete this Album ?", method: "delete"}
      else
        link_to "Delete", user_photo_path(current_user,object), class: "btn btn-outline-danger btn-block",data: { confirm: "Delete this Photo ?", method: "delete"}
      end
    end
  end

  # Show Users | Edit Admin's profile
  def admin_user_custom_path
    link_to current_user.nick_name, current_user.admin? ?
      edit_user_registration_path(current_user) : user_path(current_user),
      class: "full-name"
  end

  # Avatar User | Letters of Username
  def user_avatar(user)
    if user.avatar?
      image_tag user.avatar.url(:medium), class: "img-fluid round-image"
    else
      content_tag(:div, class: "name") do
        if user.last_name.blank?
          t('user_name',name: user.first_name.first)
        else
          t('user_name',name: user.avatar_name)
        end
      end
    end
  end

  def fotobook_logo
    current_user.admin? ? link_to(t('.logo_admin'), root_path) : link_to(t('.logo_user'), root_path)
  end

  def fotobook_menu_link
    if current_user.admin?
    " <hr />
      <a href='#{photos_path}' class=''>#{t('.admin_menu_link',link:"Manage Photos")}</a>
      <hr />
      <a href='#{albums_path}' class=''>#{t('.admin_menu_link',link:"Manage Albums")}</a>
      <hr />
      <a href='#{users_path}' class=''>#{t('.admin_menu_link',link:"Manage Users")}</a>
      <hr />".html_safe
    else
      menu_link
    end
  end

  def menu_link
  " <hr />
    <a href='#{feeds_path}' class=''>#{t('.menu_link',link: "Feeds")}</a>
    <hr />
    <a href='#{discovers_path}' class=''>#{t('.menu_link',link: "Discovers")}</a>
    <hr />".html_safe
  end

  # Feeds - Discovery (photo | album) Like
  def display_like_count(object,like_custom_path)
    if user_signed_in?
      concat link_to icon('fas', 'heartbeat'), like_custom_path, class: "vote mr-2",
              rel: "nofollow", data: { remote: "true", method: "put" }
      tag.span class: "votes-count", data: { id: object.id } do
        "#{object.likes.count}"
      end
    else
      icon('fas', 'heartbeat', "#{object.likes.count}")
    end
  end

  # Photo Modal
  def show_photo_modal(photo)
    link_to user_photo_path(photo.user_id,photo), remote: true do
      image_tag photo.image.url, class: "img-fluid"
    end
  end

  # Album Modal
  def show_album_modal(album)
    link_to user_album_path(album.user_id,album), remote: true do
      image_tag album.photos.first.image.url(:medium), class: "img-fluid"
    end
  end

  def edit_custom(object)
    if (object.user_id == current_user.id) || current_user.admin?
      content_tag(:div, class: "text-block") do
        link_to "Edit", object.class.name == "Photo"? edit_user_photo_path(current_user,object) : edit_user_album_path(current_user,object), class: "badge badge-pill badge-dark edit-btn"
      end
    end
  end

  def bootstrap_class_for flash_type
    { alert: "alert-warning", notice: "alert-success", error: "alert-danger" }.stringify_keys[flash_type.to_s] || flash_type.to_s
  end

  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      concat(content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)} text-center", role: "alert") do
        concat content_tag(:button, 'x', class: "close", data: { dismiss: 'alert' })
        concat message
      end)
    end
    nil
  end

end
