module UsersHelper

  def add_button(class_name,user_id)
    if current_user.id == user_id
      content_tag(:div, class: "col-12") do
        link_to "Add #{class_name}",
          class_name=="Photo"? new_user_photo_path(current_user) : new_user_album_path(current_user),
          class: "btn btn-outline-primary float-right"
      end
    end
  end

  def display_avatar(user,f)
    if user.avatar?
      image_tag user.avatar.url(:medium)
    else
      concat f.label :avatar
      f.file_field :avatar, id: "upload_photo"
    end
  end

  def edit_profile(user)
    if user.id == current_user.id
      content_tag(:div, class: "row") do
        link_to "Edit Profile", edit_user_registration_path(user) , class: "badge badge-pill badge-info"
      end
    end
  end

  def avatar_preview_upload(user)
    if user.avatar?
      concat(content_tag(:div, class: "col-6") do
        image_tag user.avatar.url(:medium), class: "img-thumbnail rounded"
      end)
      content_tag(:div, class: "col-6") do
        image_tag " ", class: "hidden image-to-upload"
      end
    else
      content_tag(:div, class: "col") do
        image_tag " ", class: "hidden image-to-upload"
      end
    end
  end

end
