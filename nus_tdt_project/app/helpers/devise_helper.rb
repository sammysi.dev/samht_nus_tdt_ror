module DeviseHelper

  def devise_error_messages!
    if devise_error_messages?
      messages = resource.errors.full_messages.map { |msg| content_tag(:li, msg) }.join

      html = <<-HTML
      <div class="alert alert-danger text-center" role="alert">
        <button class="close" data-dismiss="alert">x</button>
        <ul class="ul-devise">#{messages}</ul>
      </div>
      HTML

      html.html_safe
    end
  end

  def devise_error_messages?
    !resource.errors.empty?
  end

  def social_icon(provider)
    if provider == "facebook"
      tag.i class: "fab fa-facebook"
    elsif provider == "google_oauth2"
      tag.i class: "fab fa-google"
    else
      tag.i class: "fab fa-twitter"
    end
  end

end
