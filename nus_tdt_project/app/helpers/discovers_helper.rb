module DiscoversHelper

  def current_user_is_following(current_user_id, followed_user_id)
    relationship = Relationship.find_by(follower_id: current_user_id, following_id: followed_user_id)
    return true if relationship
  end

  def toggle_follow_unfollow_link(follower_id,following_id)
    if follower_id != following_id
      if current_user_is_following(follower_id,following_id)
        link_to "Following", unfollow_user_discover_path(following_id),
          rel: "nofollow",
          data: { method: "post", remote: "true" },
          class: "badge badge-pill badge-warning unfollow-#{following_id}"
      else
        link_to "Follow", follow_user_discover_path(following_id),
          rel: "nofollow",
          data: { method: "post", remote: "true" },
          class: "badge badge-pill badge-info follow-#{following_id}"
      end
    end
  end

end
