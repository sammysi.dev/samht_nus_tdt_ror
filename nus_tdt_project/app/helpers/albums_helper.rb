module AlbumsHelper

  def album_upload_file(album)
    if album.id.nil?
      file_field_tag "images[]", type: :file,
        accept: "image/png,image/gif,image/jpeg",
        required: true
    else
      file_field_tag "images[]", type: :file, id: "album_photos",
        accept: "image/png,image/gif,image/jpeg",
        multiple: true
    end
  end

  def display_album_button(album)
    if album.id?
      concat(content_tag(:div, class: "col-6") do
        save_btn
      end)
      content_tag(:div, class: "col-6") do
        delete_btn(album, "Album")
      end
    else
      content_tag(:div, class: "col-6 ml-auto") do
        save_btn
      end
    end
  end
end
