module PhotosHelper

  def photo_upload_file(photo,form)
    form.file_field :image,
      accept: "image/png,image/gif,image/jpeg",
      required: photo.id? ? false : true
  end

  def display_photo_button(photo)
    if photo.id?
      concat(content_tag(:div, class: "col-6") do
        save_btn
      end)
      content_tag(:div, class: "col-6") do
        delete_btn(photo)
      end
    else
      content_tag(:div, class: "col-6 ml-auto") do
        save_btn
      end
    end
  end

end

