class UserMailer < ApplicationMailer
  default from: "samht.nustechnology@gmail.com"

  def deactive_user_notification(id)
    @user = User.find(id)
    mail to: @user.email, subject: "Your account has been deactivated!"
  end

  def delete_user_notification(id)
    @user = User.find(id)
    mail to: @user.email, subject: "Your account has been deleted!"
  end
end
