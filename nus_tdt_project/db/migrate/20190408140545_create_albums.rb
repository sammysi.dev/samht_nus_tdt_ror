class CreateAlbums < ActiveRecord::Migration[5.2]
  def change
    create_table :albums do |t|
      t.belongs_to :user, index: true
      t.string :title, default: ""
      t.text :description, default: ""
      t.string :mode, default: "public"

      t.timestamps
    end
  end
end
