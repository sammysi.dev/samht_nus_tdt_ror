FactoryBot.define do

    factory :user do
        email { Faker::Internet.email }
        password { Faker::Internet.password }
        first_name { Faker::Internet.username }
        last_name { Faker::Internet.username }
        after(:build) { |user| user.skip_confirmation! }
    end

    factory :photo do
        mode { "public" } # default public mode
        image_file_name { Faker::Name }
        image_content_type { "image/png" }
        image_file_size { 100000 }
        image_updated_at { Time.now }
    end

    factory :album do
        mode { "public" } # default public mode
    end

    factory :relationship
end
