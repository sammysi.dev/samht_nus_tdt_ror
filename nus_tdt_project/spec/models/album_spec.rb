require 'rails_helper'
require 'factories'

RSpec.describe Album, type: :model do
  let (:user) { create(:user) }
  let (:current_user) { create(:user) }
  let (:other_user) { create(:user) }
  let (:album) { create(:album, user: user) }

  context "scope" do
    context "with descending order" do
      it "should be the newest" do
        create_list(:album, 10, user: user)
        expect( Album.newest.first.id ).to eq( Album.order(created_at: :desc).first.id )
      end

      it "should be the newest public" do
        create_list(:album, 5, user: user, mode: "private")
        create_list(:album, 10, user: user)
        expect( Album.newest_public.length ).to eq( 10 )
      end

      context "with other users' albums" do
        it "should be public" do
          create_list(:album, 5, user: current_user)
          create_list(:album, 10, user: user)
          expect( Album.newest_public_album_without(current_user).length ).to eq( 10 )
        end

        it "should be newest public" do
          create_list(:album, 5, user: current_user)
          create_list(:album, 10, user: user)
          expect( Album.newest_public_album_without(current_user).first.id ).to eq(
                  Album.includes(:photos).where.not(user: current_user).where(mode: "public").order(created_at: :desc).first.id )
        end
      end
    end

    context "with followings of an user" do
      it "should be followings' albums only" do
        create(:relationship, following: user, follower: current_user)
        create_list(:album, 10, user: user)
        create_list(:album, 5, user: other_user)
        expect( Album.following_users_of(current_user).length ).to eq(10) # not 15
      end
    end
  end

  context "validation" do
    context "with valid attributes" do
      it "should be valid" do
        expect( album ).to be_valid
      end
    end

    context "with title in user's scope" do
      it "should be valid with blank title" do
        create(:album, user: user, title: "")
        expect( create(:album, user: user, title: "") ).to be_valid
      end

      it "should be invalid with case sensitive title" do
        create(:album, user: user, title: "title")
        expect { build(:album, user: user, title: "TITLE").save! }.to raise_error(ActiveRecord::RecordInvalid)
      end

      it "should be invalid with the same title" do
        create(:album, user: user, title: "title")
        expect { build(:album, user: user, title: "title").save! }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end

    context "when invalid" do
      it "should require an user" do
        expect { build(:album).save! }.to raise_error(ActiveRecord::RecordInvalid)
      end

      it "should have title limited 140 characters" do
        expect { build(:album, user: user, title: ("?" * 141)).save! }.to raise_error(ActiveRecord::RecordInvalid)
      end

      it "should have description limited 300 characters" do
        expect { build(:album, user: user, description: ("?" * 301)).save! }.to raise_error(ActiveRecord::RecordInvalid)
      end

      it "should have public or private mode" do
        expect { build(:album, user: user, mode: "others").save! }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end
  end

  describe "#user_nick_name" do
    it "should have nick name of an user" do
      expect( album.user_nick_name ).to eq(user.nick_name)
    end
  end

  describe ".search" do
    it "should be valid" do
      album = create(:album, user: user, title: "title")
      actual = Album.search("TITLE")
      expect( actual.take.title ).to eql( album.title )
    end
  end

end
