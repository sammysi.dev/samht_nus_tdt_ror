require 'rails_helper'
require 'factories'

RSpec.describe User, type: :model do
  let(:user) { create(:user) }
  let(:current_user) { create(:user) }
  let(:other_user) { create(:user) }
  let(:photo) { create(:photo, user: user) }
  let(:album) { create(:album, user: user) }

  context "validation" do
    context "with email" do
      it "should be unique" do
        create(:user, email: "hello@example.com")
        expect { build(:user, email: "hello@example.com").save! }.to raise_error(ActiveRecord::RecordInvalid)
      end

      it "should match email format" do
        user = build(:user, email: "hello@example.com")
        expect( user.email ).to match(/\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i)
      end
    end

    context "with last name" do
      it "should be valid when using social network account" do
        expect( create(:user, last_name: "", provider: "facebook") ).to be_valid
      end
    end

    context "when invalid" do
      context "with first name & last name" do
        it "should require first name" do
          expect { build(:user, first_name: "").save! }.to raise_error(ActiveRecord::RecordInvalid)
        end

        it "should require last name" do
          expect { build(:user, last_name: "").save! }.to raise_error(ActiveRecord::RecordInvalid)
        end

        it "should have first name limited 25 characters" do
          expect { build(:user, first_name: ("?" * 26)).save! }.to raise_error(ActiveRecord::RecordInvalid)
        end

        it "should have last name limited 25 characters" do
          expect { build(:user, first_name: ("?" * 26)).save! }.to raise_error(ActiveRecord::RecordInvalid)
        end
      end

      it "should have email limited 255 characters" do
        expect { build(:user, email: ("?" * 256)).save! }.to raise_error(ActiveRecord::RecordInvalid)
      end

      it "should have password limited 64 characters" do
        expect { build(:user, password: ("?" * 65)).save! }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end
  end

  describe "#nick_name" do
    it "should have nick name" do
      expect( user.nick_name ).to eq("#{user.first_name} #{user.last_name.split(' ').first}")
    end
  end

  describe "#avatar_name" do
    it "should have avatar name" do
      expect( user.avatar_name ).to eq("#{user.first_name.first}#{user.last_name.first}")
    end
  end

  describe "#follow" do
    it "can follow other users" do
      expect( current_user.follow(user.id) ).to be_valid
    end

    context "when invalid relationship" do
      it "should require unique" do
        create(:relationship, following: user, follower: current_user)
        expect( current_user.follow(user) ).to be_invalid
      end

      it "should require following user" do
        expect { current_user.follow() }.to raise_error(ArgumentError)
      end

      it "should require follower user" do
        expect { follow(user) }.to raise_error(NoMethodError)
      end
    end
  end

  describe "#unfollow" do
    it "can unfollow other users" do
      create(:relationship, following: user, follower: current_user)
      current_user.unfollow(user)
      expect( current_user.following.include? user ).to be false
    end

    context "when invalid relationship" do
      it "should require existing following user" do
        create(:relationship, following: user, follower: current_user)
        expect( current_user.unfollow(other_user) ).to be_nil
      end
    end
  end

  describe "#like" do
    context "when likes" do
      it "should like photo" do
        expect( current_user.like(photo) ).to be_valid
      end

      it "should like album" do
        expect( current_user.like(album) ).to be_valid
      end
    end

    context "when unlikes" do
      it "should unlike photo" do
        # Like
        current_user.like(photo)
        # Unlike
        current_user.like(photo)
        expect( current_user.likes.reload.size ).to eq(0)
      end

      it "should unlike album" do
        # Like
        current_user.like(album)
        # Unlike
        current_user.like(album)
        expect( current_user.likes.reload.size ).to eq(0)
      end
    end
  end

  describe "#active_for_authentication?" do
    it "should be valid if user is active" do
      expect( user.active_for_authentication? ).to be true
    end

    it "should be invalid if user is deactive" do
      user.update(deactive: true)
      expect( user.active_for_authentication? ).to be false
    end
  end
end
