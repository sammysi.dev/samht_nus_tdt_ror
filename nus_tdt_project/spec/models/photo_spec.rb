require 'rails_helper'
require 'factories'

RSpec.describe Photo, type: :model do
  let (:user) { create(:user) }
  let (:current_user) { create(:user) }
  let (:other_user) { create(:user) }
  let (:album) { create(:album, user: user) }
  let (:photo) { create(:photo, user: user) }
  let (:photo_with_album) { create(:photo, user: user, album: album) }

  context "scope" do
    it "should be out of an album" do
      photos = Photo.no_album
      expect( photos ).to_not include (photo_with_album)
    end

    context "with descending order" do
      it "should be the newest" do
        create_list(:photo, 10, user: user)
        expect( Photo.newest.first.id ).to eq( Photo.order(created_at: :desc).first.id )
      end

      it "should be the newest public" do
        create_list(:photo, 5, user: user, mode: "private")
        create_list(:photo, 10, user: user)
        expect( Photo.newest_public.length ).to eq( 10 )
      end
      context "with other users' photos" do
        it "should be public" do
          create_list(:photo, 5, user: current_user)
          create_list(:photo, 10, user: user)
          expect( Photo.newest_public_photo_without(current_user).length ).to eq( 10 )
        end
        it "should be newest public" do
          create_list(:photo, 5, user: current_user)
          create_list(:photo, 10, user: user)
          expect( Photo.newest_public_photo_without(current_user).first.id ).to eq(
                  Photo.where(album: nil, mode: "public").where.not(user: current_user).order(created_at: :desc).first.id )
        end
      end
    end
    context "with followings of an user" do
      it "should be followings' photos only" do
        create(:relationship, following: user, follower: current_user)
        create_list(:photo, 10, user: user)
        create_list(:photo, 5, user: other_user)
        expect( Photo.following_users_of(current_user).length ).to eq(10) # not 15
      end
    end
  end

  context "validation" do
    context "with valid attributes" do
      it "should be valid out of an album" do
        expect( photo ).to be_valid
      end

      it "should be valid within an album" do
        expect( photo_with_album ).to be_valid
      end
    end

    context "with title in user's scope" do
      it "should be valid with blank title" do
        create(:photo, user: user, title: "")
        expect( create(:photo, user: user, title: "") ).to be_valid
      end

      it "should be invalid with case sensitive title" do
        create(:photo, user: user, title: "title")
        expect { build(:photo, user: user, title: "TITLE").save! }.to raise_error(ActiveRecord::RecordInvalid)
      end

      it "should be invalid with the same title" do
        create(:photo, user: user, title: "title")
        expect { build(:photo, user: user, title: "title").save! }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end

    context "when invalid" do
      it "should require an image" do
        expect { Photo.create!(user: user) }.to raise_error(ActiveRecord::RecordInvalid)
      end

      it "should require an user" do
        expect { build(:photo).save! }.to raise_error(ActiveRecord::RecordInvalid)
      end

      it "should have title limited 140 characters" do
        expect { build(:photo, user: user, title: ("?" * 141)).save! }.to raise_error(ActiveRecord::RecordInvalid)
      end

      it "should have description limited 300 characters" do
        expect { build(:photo, user: user, description: ("?" * 301)).save! }.to raise_error(ActiveRecord::RecordInvalid)
      end

      it "should have public or private mode" do
        expect { build(:photo, user: user, mode: "others").save! }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end
  end

  describe "#user_nick_name" do
    it "should have nick name of an user" do
      expect( photo.user_nick_name ).to eq(user.nick_name)
    end
  end

  describe ".search" do
    it "should be valid" do
      photo = create(:photo, user: user, title: "title")
      actual = Photo.search("TITLE")
      expect( actual.take.title ).to eql( photo.title )
    end
  end

end
