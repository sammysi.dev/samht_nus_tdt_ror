Rails.application.routes.draw do

  get 'notifications/index'
  root "feeds#index"

  # Devise
  devise_for :users, controllers: {
    registrations: "users/registrations",
    omniauth_callbacks: "users/omniauth_callbacks"
  }

  # User - Albums/Photos
  resources :users, except: [:new, :create] do
    resources :photos, except: [:index]
    resources :albums, except: [:index]
  end

  resources :photos, only: [:index] do
    resources :comments, module: :photos
    put :like, on: :member
  end

  resources :albums, only: [:index] do
    resources :comments, module: :albums
    put :like, on: :member
  end

  resources :feeds, only: [:index]

  resources :discovers, only: [:index] do
    member do
      post :follow_user
      post :unfollow_user
    end
  end

  resources :conversations, only: [:create] do
    resources :messages, only: [:create]
    member do
      post :close
    end
  end

  mount ActionCable.server => '/cable'

end
