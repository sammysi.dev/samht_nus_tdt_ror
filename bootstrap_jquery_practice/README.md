Before Press [AC]<br />
-> [Clr] disabled.<br />
-> [=] display "Please press AC". ([Del] can not delete this String)<br />
-> Others do not work.<br />

After Press [AC]<br />
-> [Clr] enabled.<br />
-> [Clr] : clear history equations and focus input screen.<br />
-> Screen is focus.<br />
when screen has nothing (length = 0)<br />
---------> input "=" : focus screen<br />
---------> [Del] : focus screen<br />
---------> input number : return number<br />
---------> input operators : return<br />

When screen has length = 1 (numbers or operators)<br />
---------> [Del] : delete and focus.<br />

**warning**<br />
When press "=" the value will be reset.<br />
When press "AC" 2nd time will turn off screen and delete history.<br />
Number/0 -> Infinity<br />
Undefined equation is not added to history<br />
--------------------------------------------------------------------
Expand shows 4 buttons slideToggle slow
