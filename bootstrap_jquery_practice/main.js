var input = document.getElementById("screen");
var mySelect = document.getElementById("history");

//Expand function
$(document).ready(function(){
    $("button#expand").click(function(){
        $(".expandbelow").slideToggle("slow");
    });
});

//Select function
function displayVals() {
	var value = $("#history").val();
	$("#screen").val(eval(value));
}
$("select").change(displayVals);
displayVals();

//Insert function
function insert(element){
	var result = element.innerHTML;
	if(result == "="){
		if(input.placeholder == ""){
			$("input").prop('disabled', false);
			input.placeholder = "Please press AC";
		}
		else if(input.placeholder == " "){
			$("input#screen").focus();
		}
		else if(input.placeholder != "0"){
			var myOption = document.createElement("option");
			if(eval(input.placeholder) != undefined){
				myOption.text = input.placeholder;
				mySelect.add(myOption);
				input.value = eval(input.placeholder);
				input.placeholder = "  ";
			}
		}
	}
	else if(result == "AC"){
		//enable input
		$("input").prop('disabled', false);
		$("#btnModal").prop('disabled', false);
		//disable keyboard input
		$(document).keypress(function(e) {
		    return false;
		});
		//focus screen
		$("input#screen").focus();

		input.placeholder = " ";
		var currentvalue = $(this).attr("value");
		$(this).attr("value","on");
		if(currentvalue == "on"){
			$("input").prop('disabled', true);
			$("#btnModal").prop('disabled', true);
			input.value = "";
			input.placeholder = "";
			$(this).attr("value","off");
			$("#history").empty();
		}
	}
	else if(result == "Del"){
		if(input.placeholder != "Please press AC"){
			input.value = "";
			var x = input.placeholder;
			if(x.length > 0){
				x = x.trim();
				x = x.substring(0,x.length-1);
				input.placeholder = x;
				if(x.length == 0){
					$("input#screen").focus();
					input.placeholder = " ";
				}
			}
		}
	}
	else if(result == "Close"){
		$('#Modal').on('hidden.bs.modal', function() {
	      $('input#screen').focus();
	    });
	}
	else if(result == "Clear"){
		input.placeholder = " ";
		input.value = "";
		$("#history").empty();
		$('#Modal').on('hidden.bs.modal', function() {
	      $('input#screen').focus();
	    });
	}
	else if(result == "0"){
		if(input.placeholder != "")
			input.value = "";
		if(result == "+" || result == "-" || result == "*" || result == "/")
			input.placeholder += " "+result+" ";
		else
			input.placeholder += result;
	}
	else{
		if(input.placeholder != "Please press AC" && input.placeholder != ""){
			input.value = "";
			if(input.placeholder == "0" || input.placeholder == " " || input.placeholder == "  "){
				if(result == "+" || result == "-" || result == "*" || result == "/")
					input.placeholder = "0";
				else
					input.placeholder = result;
			}
			else{
				if(result == "+" || result == "-" || result == "*" || result == "/")
					input.placeholder += " "+result+" ";
				else
					input.placeholder += result;
			}
		}
	}
}