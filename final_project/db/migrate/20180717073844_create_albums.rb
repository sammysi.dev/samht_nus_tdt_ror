class CreateAlbums < ActiveRecord::Migration[5.2]
  def change
    create_table :albums do |t|
      t.belongs_to :user, index: true
      t.string :title
      t.text :description
      t.boolean :mode, default: false

      t.timestamps
    end
  end
end
