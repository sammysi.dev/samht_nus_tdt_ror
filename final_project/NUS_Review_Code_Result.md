# NUS REVIEW CODE RESULT FOR VI's FINAL APP

### Issues
---------

#### Architecture


#### Security

1. Với cách implement hiện nay thì 1 user có thể dễ dàng chỉnh sửa hoặc xóa album/photo của user khác. Đây là 1 điều khá nghiêm trọng với 1 ứng dụng.

#### Performance

1. App có khá nhiều N+1 issues. Xem rails log hoặc dùng gem `bullet` để detect.

#### Coding conventions & best practices

1. Coding style không đồng nhất, có phần khó nhìn.

    - Indent lúc thì 2 space lúc thì 4 spaces. Best practice bây giờ hầu hết các ngôn ngữ đều sử dụng 2 spaces
    - Nên có break line giữa các methods cho dễ nhìn

2. Ưu tiên dùng coffee hơn js để giảm thời gian develop.

3. Các chức năng JS cần cho page nào thì nên được thực thi cho page đó thôi. Chỉ có những chức năng dùng ở nhiều trang hoặc tất cả các trang thì mới nên được tổ chức như hiện nay. Cách tổ chức code JS hiện nay rất dễ gây xung đột.

4. Không tận dụng được sức mạnh của SCSS để làm gọn code. Đang viết SCSS mà syntax viết cũng y hệt CSS.

5. Khi dùng devise, những controllers nào không cần customize thì không cần tạo ra những controllers đó và chỉ định lại trong routes. Hiện nay thấy `passwords` và `sessions` là đang dư.

6. Nhiều chỗ không tận dụng được sức mạnh của `association` và `scope` nên làm code dài dòng và trùng lặp. Ví dụ:

    - AlbumsController#index

    `Album.includes(:users, :likes).where("user_id = ?",current_user.id).order('created_at DESC').page(params[:page]).per(20)`
    => `current_user.albums.includes(:users, :likes).order('created_at DESC').page(params[:page]).per(20)`
    => Việc `order` album được sử dụng nhiều nơi nên tạo scope cho nó => `current_user.albums.includes(:users, :likes).newest_first.page(params[:page]).per(20)` => có thể tận dụng scope này cho đoạn code phía trên đoạn và những nơi khác.

    - AlbumsController#create

    ```
    @album = Album.new album_params
    @album.user_id = current_user.id
    ```

    => `@album = current_user.albums.new(album_params)`

    - FeedsController#index

    ```
    @photos = Photo.includes(:users, :likes, :album).where('mode = ?', false).order('created_at DESC').search(params[:search]).page(params[:page]).per(20)

    @albums = Album.includes(:users, :likes).where('mode = ?', false).order('created_at DESC').search(params[:search]).page(params[:page]).per(20)
    ```

    =>

    ```
    @photos = Photo.includes(:users, :likes, :album).public.new_first.search(params[:search]).page(params[:page]).per(20)

    @albums = Album.includes(:users, :likes).public.new_first.search(params[:search]).page(params[:page]).per(20)
    ```

    - PhotosController#index

    ```
    if params[:album_id]
      @photos = Photo.includes(:users, :album, :likes).where('user_id = ? and album_id = ?',current_user.id, params[:album_id]).order('created_at DESC').page(params[:page]).per(20)
    else
      @photos = Photo.includes(:users, :likes).where('user_id = ?',current_user.id).order('created_at DESC').page(params[:page]).per(20)
    end
    ```

    =>

    ```
    @photos = current_user.photos.includes(:users, :likes).new_first.page(params[:page]).per(20)
    @photos = @photos.where(album_id: params[:album_id]) if params[:album_id]
    ```

7. Quá lạm dụng helper dẫn đến tình trạng sisnh ra những helper methods không cần thiết như:

    - mode_option1_album
    - mode_option2_album
    - sign_io_icon
    - sign_in_out
    - plus(i)

8. Dùng string interpolation thay vì cộng chuỗi

    - app/models/user.rb#full_name


    ```
    last_name + " " + first_name
    ```

    => `"#{last_name} #{first_name}"`

    - app/models/user.rb#nick_name


9. Với những method đơn giản mà đọc vào ai cũng hiểu chức năng của nó làm gì thì việc thêm comment là dư thừa ko cần thiết.

    - app/models/user.rb#full_name
    - app/models/user.rb#nick_name
    - etc.


10. Code của Photo và Album có khá nhiều cái giống nhau (validation, scope, etc). Tìm hiểu về module + rails model concern để gom code lại tránh việc duplicate code.

11. Đối với việc format ngày tháng ko nên dùng `strftime("%b %d, %Y")` vì khi cần sửa format thì phải kiếm tất cả các chỗ dùng để sửa. Tìm hiểu về I18n.localize (viết tắt I18n.l) để format date/time thay thế cho hàm `strftime`

12. Ưủ tiên dùng rails helper thay vì native html

    - app/views/albums/edit.html.erb

    ```
    <input type="submit" id="editupload" class="btn btn-outline-success float-right mb-3 save-btn" value="Save">
    ```

    =>

    ```
    <%= f.submit 'Save', id: 'editupload', class: 'btn btn-outline-success float-right mb-3 save-btn' %>
    ```

    ```
    <a data-confirm="Delete this album?" rel="nofollow" data-method="delete" href="/albums/<%= @album.id %>"><i class="btn btn-outline-danger fas fa-trash-alt"> Delete album</i></a>
    ```

    =>

    ```
    <%= link_to album_path(@album), rel: 'nofollow', data: {method: 'delete', confirm: 'Delete this album?'} do %>
      <i class="btn btn-outline-danger fas fa-trash-alt"> Delete album</i>
    <% end %>
    ```

    - etc.

13. Không được harde code các path, dùng path helper để sinh ra path mình cần

    - app/views/albums/index.html.erb

    ```
    <a href="/albums/<%= album.id %>/edit" class="btn btn-outline-success fas fa-edit float-right"></a>
    ```

    => `link_to edit_album_path(album), class: ...` or `a href="<%= edit_album_path(album) %>"`


14. Không nên dùng biến @ trong partial. Partial cần gì đề works thì truyền vào khi render

    - app/views/feeds/_fphoto.html.erb
    - app/views/feeds/_falbum.html.erb


15. Tránh sử dụng asset qua CDN, nên download về rồi dùng asset pipeline để bỏ vào app. Ví dụ:

    - `<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>`
    - `<link href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css" rel="stylesheet">`

#### Bugs or need to be enhanced

1. Giao diện chưa tốt so với design.

2. Dưới phương diện là 1 trainee, nên tự làm chức năng admin thay vì dùng gem. Như vậy sẽ tích lũy được nhiều kinh nghiệm hơn.

3. Chức năng admin hiện nay bất cứ users nào vào cũng được, như vậy không ổn. Với vị trí developer luôn luôn cần phải có sự quan tâm đặc biệt đến security.

4. Code khá "tào lao"

    - app/views/albums/edit.html.erb

    ```
    <select name="album[mode]" id="album_mode" class="form-control">
    <%= mode_optión_album(@album) %><%= mode_optiòn_album(@album) %>
    </select>
    ```

    => Research để dùng được f.select thay vì viết như trên.

#### Conclusion

Source code nhìn chung thì chưa được tốt lắm, cần phải cải thiện nhiều. Nhưng với xuất phát điểm tự train là chính và ko có người kèm cặp thì mức độ hoàn thiện của final như vậy là chấp nhận được, cho thấy được sự có nỗ lực.