Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  get 'feeds/index'
  root 'feeds#index'
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    passwords: 'users/passwords',
    registrations: 'users/registrations',
    omniauth_callbacks: "omniauth_callbacks"
  }

  resources :users

  resources :photos do
    member do
      patch :like
      put :like
    end
  end

  resources :albums do
    resources :photos
    member do
      patch :like
      put :like
    end
  end

end
