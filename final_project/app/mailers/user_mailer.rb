class UserMailer < ActionMailer::Base
  default from: "sammysi.dev@gmail.com"

  def user_deactivated(id)
    @user = User.find(id)
    mail to: @user.email, subject: "Your account has been deactivated!"
  end

  def user_deleted(id)
    @user = User.find(id)
    mail to: @user.email, subject: "Your account has been deleted!"
  end

end