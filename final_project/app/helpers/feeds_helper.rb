module FeedsHelper

  def h6_class(photo)
    if photo.user.avatar.exists?
      "p-ava-margin fix-right-frame"
    else
      "p-n-ava-margin fix-n-right-frame"
    end
  end

  def feed_user_avatar(photo)
    if photo.user.avatar.exists?

      tag.img src: "#{photo.user.avatar.url(:medium)}", width: "45", height: "45", class: "p-ava d-inline-block align-top"

    else

      tag.span class: "p-n-ava" do
        "#{photo.user.first_name.first}" + "#{photo.user.last_name.first}"
      end

    end
  end

  def feed_user_name(photo)
    if photo.user.nick_name.length > 8
      tag.span class: "none-ava" do
        " #{photo.user.nick_name.truncate(8)}"
      end
    else
      tag.span class: "none-ava" do
        " #{photo.user.nick_name}"
      end
    end
  end

  def h6_class_album(album)
    if album.user.avatar.exists?
      "mb-45"
    else
      ""
    end
  end

  def feed_user_album(album)
    if album.user.avatar.exists?

      tag.img src: "#{album.user.avatar.url(:medium)}", width: "45", height: "33", class: "d-inline-block align-top a-ava"

    else

      tag.span class: "p-a-ava" do
        "#{album.user.first_name.first}" + "#{album.user.last_name.first}"
      end

    end
  end

  def feed_user_name_album(album)
    if album.user.nick_name.length > 16
      tag.span class: "none-ava" do
        " #{album.user.nick_name.truncate(16)}"
      end
    else
      tag.span class: "none-ava" do
        " #{album.user.nick_name}"
      end
    end
  end

end
