module UsersHelper

  def display_name_avatar(user)
    if user.avatar.exists?

      tag.img src: "#{user.avatar.url(:medium)}", width: "45", height: "40", class: "u-ava d-inline-block align-top"

    else

      tag.span class: "p-n-ava" do
        "#{user.first_name.first}" + "#{user.last_name.first}"
      end

    end
  end

  def class_avatar(user)
    if user.avatar.exists?
      "col-12"
    else
      "col-12 pt-4 mb-3"
    end
  end

  def display_avatar(user)
    if user.avatar.exists?

      tag.img src: "#{user.avatar.url(:medium)}", class: "avaimg"

    else

      tag.img alt: "your avatar", class: "avaimg hidden", src: "#"

    end
  end

  def plus(i)
    i + 1
  end

end