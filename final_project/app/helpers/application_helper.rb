module ApplicationHelper

  def bootstrap_class_for flash_type
    { alert: "alert-danger", notice: " alert-success" }.stringify_keys[flash_type.to_s] || flash_type.to_s
  end

  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      concat(content_tag(:p, message, class: "alert #{bootstrap_class_for(msg_type)} text-center", role: "alert") do
        concat content_tag(:button, 'x', class: "close", data: { dismiss: 'alert' })
        concat message
      end)
    end
    nil
  end

  def title_view
    if user_signed_in?

      if current_user.admin_role?
        "CooLPhoto Admin"
      else
        "CooLPhoto"
      end

    else
      "CooLPhoto"
    end
  end

  def user_name_avatar(user)
    if user_signed_in?

      user.full_name

    end
  end

  def sign_io_icon
    if user_signed_in?

      tag.a tabindex: "-1", data: { toggle: "modal", target: "#modal"} do
        tag.span class: "color-nav" do
          tag.i class: "fas fa-sign-out-alt"
        end
      end

    else

      tag.a href: "/users/sign_in" do
        tag.span class: "color-nav" do
          tag.i class: "fas fa-sign-in-alt"
        end
      end

    end
  end

  def sign_in_out
    if user_signed_in?

      tag.a tabindex: "-1", data: { toggle: "modal", target: "#modal"} do
        tag.span class: "color-nav" do
          " Sign out"
        end
      end

    else

      tag.a href: "/users/sign_in" do
        tag.span class: "color-nav" do
          " Sign in"
        end
      end

    end
  end

  def edit_user_info(user)
    if user_signed_in?
      "/#{user.id}/"
    else
      "/"
    end
  end

  def avatar_user(user)

    if user_signed_in?
      if current_user.avatar.exists?

        tag.img src: "#{current_user.avatar.url(:medium)}", width: "40", height: "30", class: "d-inline-block align-top user-avatar"

      else

        tag.span class: "user-avatar-span" do
          user.first_name.first + user.last_name.first
        end

      end
    end

  end

  def menu_app1(user)
    if current_user.admin_role?

      tag.a href: "/photos", class: "link-none" do
        tag.span class:"text-style" do
          "Manage Photos"
        end
      end

    else

      tag.a href: "/", class: "link-none" do
        tag.span class:"text-style" do
          "Feeds"
        end
      end

    end
  end

  def menu_app2(user)
    if current_user.admin_role?

      tag.a href: "/albums", class: "link-none" do
        tag.span class:"text-style" do
          "Manage Albums"
        end
      end

    else

      tag.a href: "/photos", class: "link-none" do
        tag.span class:"text-style" do
          "My Photos"
        end
      end

    end
  end

  def menu_app3(user)
    if current_user.admin_role?

      tag.a href: "/users", class: "link-none" do
        tag.span class:"text-style" do
          "Manage Users"
        end
      end

    else

      tag.a href: "/albums", class: "link-none" do
        tag.span class:"text-style" do
          "My Albums"
        end
      end

    end
  end

end
