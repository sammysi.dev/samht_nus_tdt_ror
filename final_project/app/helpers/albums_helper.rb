module AlbumsHelper

  def mode_album(album)
    if album.mode == true

      tag.i class: "fas fa-lock"

    end
  end

  def mode_option1_album(album)
    if album.mode == true

      tag.option value: "true" do
        "private"
      end

    else

      tag.option value: "false" do
        "public"
      end

    end
  end

  def mode_option2_album(album)
    if album.mode == true

      tag.option value: "false" do
        "public"
      end

    else

      tag.option value: "true" do
        "private"
      end

    end
  end

  def top_page
    if current_user.admin_role?

      tag.h5 class: "p-a-title" do
        "All Albums"
      end

    else

      tag.div class: "row" do
        tag.div class: "col-12" do
          tag.a class: "btn btn-add float-right", href: "/albums/new" do
            "New album"
          end
        end
      end

    end
  end

end
