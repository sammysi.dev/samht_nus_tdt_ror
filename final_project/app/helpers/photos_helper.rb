module PhotosHelper

  def top_page_photo
    if current_user.admin_role?

      tag.h5 class: "p-a-title" do
        "All Photos"
      end

    else

      tag.div class: "row" do
        tag.div class: "col-12" do
          if params[:album_id]
            tag.a class: "btn btn-add float-right", href: "/albums/#{params[:album_id]}/photos/new" do
              "New photo"
            end
          else
            tag.a class: "btn btn-add float-right", href: "photos/new" do
              "New photo"
            end
          end
        end
      end

    end
  end

  def mode_option1_photo(photo)
    if photo.mode == true

      tag.option value: "true" do
        "private"
      end

    else

      tag.option value: "false" do
        "public"
      end

    end
  end

  def mode_option2_photo(photo)
    if photo.mode == true

      tag.option value: "false" do
        "public"
      end

    else

      tag.option value: "true" do
        "private"
      end

    end
  end

  def del_btn_edit_photo(photo)
    if photo.image.exists?

      tag.a rel: "nofollow", href: "/photos/#{photo.id}", data: { confirm: "Delete this photo?", method: "delete" } do
        tag.i class: "btn btn-outline-danger fas fa-trash-alt" do
          "Delete"
        end
      end

    end
  end

  def save_btn_edit_photo(photo)
    tag.input type: "submit", class: "btn btn-outline-success float-right save-btn", value: "Save"
  end

end

