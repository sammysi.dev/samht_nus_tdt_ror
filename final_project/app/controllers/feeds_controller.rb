class FeedsController < ApplicationController
  skip_before_action :authenticate_user!

  def index
    @photos = Photo.includes(:users, :likes, :album).where('mode = ?', false).order('created_at DESC').search(params[:search]).page(params[:page]).per(20)

    @albums = Album.includes(:users, :likes).where('mode = ?', false).order('created_at DESC').search(params[:search]).page(params[:page]).per(20)
  end
end
