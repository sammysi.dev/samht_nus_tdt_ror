class UsersController < ApplicationController
  before_action :load_user, only: [:edit, :update, :destroy]

  def index
    # Display all users for admin role
    @users = User.where("id != ?",current_user.id)
  end

  def update

    if @user.update user_params
      send_mail_once_if_deactivated
      flash[:notice] = "User was successfully updated."
      redirect_to "/"
    else
      render :edit
    end

  end

  def destroy
    send_user_delete_email(@user.id)
    @user.destroy
    flash[:notice] = "User was successfully deleted."
    redirect_to users_path
  end

  private

    def load_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:first_name, :last_name, :password, :email, :avatar, :deactivated)
    end

    #Send mail if user is deactivated at once
    def send_mail_once_if_deactivated
      if @user.deactivated?
        send_user_deactive_email(@user.id) if user_params.has_key?(:deactivated)
      end
    end

    #Send mail to user after being deactivated
    def send_user_deactive_email(id)
      UserMailer.user_deactivated(id).deliver
    end

    #Send mail to user after being deleted
    def send_user_delete_email(id)
      UserMailer.user_deleted(id).deliver
    end

end
