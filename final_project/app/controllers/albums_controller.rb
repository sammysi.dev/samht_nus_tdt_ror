class AlbumsController < ApplicationController
  before_action :load_album, only: [:edit, :update, :destroy, :like]
  before_action :load_album_create, only: [:new, :create]

  def index

    if current_user.admin_role?
      # Display albums for admin
      @albums = Album.includes(:users, :likes).order('created_at DESC').page(params[:page]).per(40)

    else
    # Display albums for normal users
      @albums = Album.includes(:users, :likes).where("user_id = ?",current_user.id).order('created_at DESC').page(params[:page]).per(20)

    end

  end

  def create

    @album = Album.new album_params
    @album.user_id = current_user.id

    if @album.save

      if params[:images]
      # User only create an album with one photo
        params[:images].each { |image|
          @album.photos.create(image: image, user: current_user, title: @album.title, description: @album.description)
        }
      end

      # After that user is allowed to upload multiple photos at once
      flash[:notice] = "Enable to upload multiple photos! (only 25 photos for one album)"
      redirect_to edit_album_path(@album.id)

    else
      render :new
    end

  end

  def update

    if @album.update album_params

      if params[:images]
        # Update images for both admin and normal users
        params[:images].each { |image|
          @album.photos.create(image: image, user: @user, title: @album.title, description: @album.description)
        }

      elsif params[:delete_ids]
        #Destroy images by an array of photo[:id]
        ids = params[:delete_ids].split(",")
        Photo.includes(:album).where(:album_id => @album.id).where.not(:id => ids).delete_all

      end

      flash[:notice] = "Album was successfully updated."
      redirect_to albums_path

    else
      render :edit
    end

  end

  def destroy

    @album.destroy
    flash[:notice] = "Album was deleted."
    redirect_to albums_path

  end

  def like

    current_user.like(@album)

    if request.xhr?
      render json: { count: @album.likes.count, id: params[:id] }
    else
      redirect_to @album
    end

  end

  private

    def album_params
      params.require(:album).permit(:title, :description, :photos, :mode, :delete_ids)
    end

    def load_album_create
      @album = Album.where('user_id = ?',current_user.id).new
    end

    def load_album
      @album = Album.find(params[:id])
      @user = User.find(@album.user_id) #return user_id of album when admin updates
    end

end
