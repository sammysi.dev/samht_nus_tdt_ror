class PhotosController < ApplicationController
  before_action :load_photo, only: [:edit, :update, :destroy, :like]
  before_action :load_photo_create, only: [:new, :create]

  def index
    if current_user.admin_role?
      # Display photos for admin with or without album_id
      if params[:album_id]
        @photos = Photo.includes(:album, :likes).where('album_id = ?', params[:album_id]).order('created_at DESC').page(params[:page]).per(20)
      else
        @photos = Photo.includes(:users, :album, :likes).order('created_at DESC').page(params[:page]).per(40)
      end

    elsif current_user.user_role?
      # Display photos for normal users with or without album_id
      if params[:album_id]
        @photos = Photo.includes(:users, :album, :likes).where('user_id = ? and album_id = ?',current_user.id, params[:album_id]).order('created_at DESC').page(params[:page]).per(20)
      else
        @photos = Photo.includes(:users, :likes).where('user_id = ?',current_user.id).order('created_at DESC').page(params[:page]).per(20)
      end

    end
  end

  def create

    @photo = Photo.new(photo_params)
    @photo.user_id = current_user.id

    if @photo.save
      flash[:notice] = "Photo was successfully created."
      redirect_to photos_path
    else
      render :new
    end

  end

  def update

    if @photo.update photo_params
      flash[:notice] = "Photo was successfully updated."
      redirect_to photos_path
    else
      render :edit
    end

  end

  def destroy

    if @photo.album_id?
      # Check if album only has one photo
      if @photo.check_album_photos > 1
        destroy_user
      else
        flash[:alert] = "Album must have 1 photo at least!"
        redirect_to photos_path
      end

    else
      destroy_user
    end

  end

  def like

    current_user.like(@photo)

    if request.xhr?
      render json: { count: @photo.likes.count, id: params[:id] }
    else
      redirect_to @photo
    end

  end

  private

    def destroy_user
      @photo.destroy
      flash[:notice] = "Photo was deleted."
      redirect_to photos_path
    end

    def photo_params
      params.require(:photo).permit(:album_id, :title, :description, :mode, :image)
    end

    def load_photo
      @photo = Photo.find(params[:id])
    end

    def load_photo_create
      if params[:album_id]
        @photo = Photo.where(:album_id => params[:album_id], :user_id => current_user.id).new
      else
        @photo = Photo.where(:user_id => current_user.id).new
      end
    end

end
