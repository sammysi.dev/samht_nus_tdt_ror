class Photo < ApplicationRecord
  include Paperclip::Glue
  validate :length_of_album_images, on: :create

  #Associations
  belongs_to :user
  belongs_to :album, optional: true
  has_many :likes, :as => :likeable, :dependent => :destroy
  has_many :users, :through => :likes

  #General
  validates :title, :description, :image, presence: true

  #Attachment
  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: ["image/jpeg", "image/gif", "image/png"]
  validates_with AttachmentSizeValidator, attributes: :image, less_than: 5.megabytes

  #Maxlength
  validates :title, length: { maximum: 140,
    too_long: "%{count} characters is the maximum allowed" }
  validates :description, length: { maximum: 300,
    too_long: "%{count} characters is the maximum allowed" }

  #Album contains maximum 25 images despite uploading more than the limitation
  def length_of_album_images
    if album_id
      errors.add(:album,"contains 25 images for every album.") if Photo.includes(:album, :likes).where(:album_id => album_id).count > 24
    end
  end

  #Roll back if album just has one image (can not destroy album with one image)
  def check_album_photos
    Photo.where(:album_id => album_id).count if album_id
  end

  def self.search(search)
    if search
      where('title LIKE ?', "%#{search}%")
    else
      all
    end
  end

end
