class Album < ApplicationRecord
  include Paperclip::Glue

  #Associations
  belongs_to :user
  has_many :photos, :dependent => :destroy
  has_many :likes, :as => :likeable, :dependent => :destroy
  has_many :users, :through => :likes

  #General
  validates :title, :description, presence: true

  #Uniqueness
  validates :title, uniqueness: { scope: :user_id, case_sensitive: false }

  #Maxlength
  validates :title, length: { maximum: 140,
    too_long: "%{count} characters is the maximum allowed" }
  validates :description, length: { maximum: 300,
    too_long: "%{count} characters is the maximum allowed" }

  private

    def self.search(search)
      if search
        where('title LIKE ?', "%#{search}%")
      else
        all
      end
    end

end
