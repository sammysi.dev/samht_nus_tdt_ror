class User < ApplicationRecord
  include Paperclip::Glue
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable, :confirmable, :omniauthable

  #Associations
  has_many :photos, :dependent => :destroy
  has_many :albums, :dependent => :destroy
  has_many :likes, :dependent => :destroy
  has_many :liked_photos, :through => :likes, :source => :likeable, :source_type => 'Photo'
  has_many :liked_albums, :through => :likes, :source => :likeable, :source_type => 'Album'

  #Email
  validates :email, uniqueness: true
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create

  #Avatar
  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :avatar, content_type: ["image/jpeg", "image/png"]
  validates_with AttachmentSizeValidator, attributes: :avatar, less_than: 2.megabytes

  #Presence
  validates :email, :first_name, :last_name, presence: true

  #Maxlength
  validates :first_name, :last_name, length: { maximum: 25,
    too_long: "%{count} characters is the maximum allowed" }
  validates :email, length: { maximum: 255,
    too_long: "%{count} characters is the maximum allowed" }
  validates :password, length: { maximum: 64,
    too_long: "%{count} characters is the maximum allowed" }

  #Like Photo/Album
  def like(item)
    if likes.where(:likeable_id => item.id, :likeable_type => item.class, :user_id => id).exists?
      Like.destroy(likes.where(:likeable_type => item.class, :likeable_id => item.id, :user_id => id).ids)
    else
      likes.create(likeable: item)
    end
  end

  #Deactived users can not sign in - normal account or FB/GG account
  def active_for_authentication?
    super && !deactivated?
  end

  #Full_name display
  def full_name
    last_name + " " + first_name
  end

  #Nick_name display
  def nick_name
    full_name.split(' ').last + " " + full_name.split(' ').first
  end

  private

    #Social login
    def self.from_omniauth(auth, signed_in_resource = nil)
      user = User.where(provider: auth.provider, uid: auth.uid).first
      if user.present?
        user
      else
        # Check wether theres is already a user with the same
        # email address
        user_with_email = User.find_by_email(auth.info.email)
        if user_with_email.present?
          user = user_with_email
        else
          user = User.new
          if auth.provider == "facebook"
            user.provider = auth.provider
            user.uid = auth.uid
            user.oauth_token = auth.credentials.token

            user.first_name = auth.extra.raw_info.first_name
            user.last_name = auth.extra.raw_info.last_name
            user.email = auth.extra.raw_info.email
            # Facebook's token doesn't last forever
            user.oauth_expires_at = Time.at(auth.credentials.expires_at)
            user.skip_confirmation!
            user.save

          elsif auth.provider == "google_oauth2"
            user.provider = auth.provider
            user.uid = auth.uid
            user.oauth_token = auth.credentials.token

            user.first_name = auth.info.first_name
            user.last_name = auth.info.last_name
            user.email = auth.info.email
            # Google's token doesn't last forever
            user.oauth_expires_at = Time.at(auth.credentials.expires_at)
            user.skip_confirmation!
            user.save
          end
        end
      end
      return user
    end

    # For Twitter (save the session eventhough we redirect user to registration page first)
    def self.new_with_session(params, session)
      if session["devise.user_attributes"]
        new(session["devise.user_attributes"], without_protection: true) do |user|
          user.attributes = params
          user.valid?
        end
      else
        super
      end
    end

    # For Twitter (disable password validation)
    def password_required?
      super && provider.blank?
    end

end
