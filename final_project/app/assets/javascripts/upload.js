$(document).on('turbolinks:load', function() {
  $('#upload').bind("click",function()
  {
    // Check if user does not upload any photos in album [:create]
      var imgVal = $('#images_').val();
      if (!imgVal){
        alert("Please upload an image!");
        return false;
      }
  });
  $('#photoupload').bind("click",function()
  {
    // Check if user does not upload any images in photo [:create]
      var imgVal = $('#photo_image').val();
      if(!imgVal){
        alert("Please upload an image!");
        return false;
      }
  });
});