// Disable password fields if blank for ADMIN
$(".password-admin-fields").each(function() {
  var $fields, $form;
  $form = $(this).parents("form");
  $fields = $(this).find("input");
  $form.on("submit", function() {
    $fields.each(function() {
      if ($(this).val() === "") {
        $(this).attr("disabled", "disabled");
      }
    });
  });
});
// Update without current_password for Omniauth accounts
$(".password-fields").each(function () {
  var $fields, $button, html;
  $fields = $(this);
  $button = $fields.prev(".update-password").find("input");
  html = $fields.html();

  $button
    .on("change", function () {
      if ( $(this).is(":checked") ) {
        $fields.html(html);
        $(".update-password").addClass("hidden")
        $("#u-deact").addClass("hidden")
      }
      else {
        $fields.html("");
      }
    })
    .prop("checked", "checked")
    .click();
});