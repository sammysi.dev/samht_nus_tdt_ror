//Photo modal
$('#mainModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var title = button.data('title') // Extract info from data-* attributes
  var description = button.data('description')
  var image = button.data('image')
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-title').text(title)
  modal.find('.footer-image').text(description)
  modal.find('.modal-body').html('<img class="photo-modal" src="' + image + '">')
})
//-----------------------------------------------------------------------------------------
//Preview image
function readURL() {
  var $input = $(this);
  var $newinput =  $(this).parent().parent().parent().find('.portimg ');
  var $avatar =  $(this).parent().parent().parent().find('.avaimg ');
  if (this.files && this.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      reset($newinput.next('.delbtn'), true);
      $newinput.removeClass( "hidden" );
      $newinput.attr('src', e.target.result).show();
      $avatar.attr('src', e.target.result).show();
      $newinput.after('<span class="delbtn removebtn"></span>');
    }
    reader.readAsDataURL(this.files[0]);
  }
}
$(".fileUpload").change(readURL);
$("form").on('click', '.delbtn', function (e) {
    reset($(this));
});

function reset(elm, prserveFileName) {
  if (elm && elm.length) {
    var $input = elm;
    $input.prev('.portimg').attr('src', '').hide();
    if (!prserveFileName) {
        $('input.fileUpload').val("");
    }
    elm.remove();
  }
}
//-----------------------------------------------------------------------------------------
//Slideshow album
baguetteBox.run('.tz-gallery', {
  animation: 'slideIn'
});
//-----------------------------------------------------------------------------------------
//Preview image - edit album
$('.remove_img_preview').on("click", function () {
  $(this).parent('.preview').remove();
  $("#delete_ids").val("");
});

function handleMultipleSelect(evt) {
  var files = evt.target.files; // FileList object

  // Loop through the FileList and render image files as thumbnails.
  for (var i = 0, f; f = files[i]; i++) {

    // Only process image files.
    if (!f.type.match('image.*')) {
      continue;
    }

    var reader = new FileReader();

    // Closure to capture the file information.
    reader.onload = (function(theFile) {
      return function(e) {
        // Render thumbnail.
        var span = document.createElement('span');
        span.classList.add("upload");
        var label = document.getElementById('label-image');
        span.innerHTML = ['<img class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/><span class="remove_img_preview"></span>'].join('');
        document.getElementById('list').insertBefore(span, label);
        $('.remove_img_preview').on("click", function () {
          $('span.upload').attr('src', '').hide();
          $('input#edit').val("");
        });
      };
    })(f);

    // Read in the image file as a data URL.
    reader.readAsDataURL(f);
  }
}
document.getElementById('edit').addEventListener('change', handleMultipleSelect, false);

//-----------------------------------------------------------------------------------------
//Sign up user - password user (Devise)
$(".new_user").validate({
    errorPlacement: function (error, element) {
      error.insertAfter(element);
    },
    debug: true,
    rules: {},
    messages: {},
    onfocusout: function(element) {
      this.element(element);
    },
    submitHandler: function(form) {
      form.submit();
    }
  });