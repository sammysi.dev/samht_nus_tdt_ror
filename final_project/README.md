# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Test Users - ID: tom_hdpetbg_selig@tfbnw.net || Password: coolphoto (FB)
* Test Users - ID: alice_ruyyvft_wonderland@tfbnw.net || Password: coolphoto (FB)

* Responsive layout - all screens based on bootstrap4

* Best practices except 2, 11, 13, 17, 18

* All requirements done. All files have comments

* Database mysqli2 - localhost

* Testing layout and requirements manually

* Omniauth and normal users can update profile without password needed && update a new password without current password and password confirmation

* Layout "update profile" of admin is different from others (Active user - Deactive user)

* Create normal user needs email confirmation && Login social accounts does not need confirmation

* After updating password successfully, user signs out

* ...
